/*
 * modes.c
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Handle switching windows between modes (Main App/Applet)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/Xatom.h>

#include "anduin.h"

//---------------------------------------------------------
// remove window from applet or main app context
void unassignWin(AppWin *appWin, int touchWin) {
	XWindowChanges xwc;

	if(rVERBOSE) tickerMsg("Unassigning Window");
	if(appWin == NULL)
		return;

	if(appWin->mode == UNASSIGNED) 
		return;
	
	if(touchWin) {
		if(! appWin->inputOnly) {
			xwc.border_width = 1;
			XConfigureWindow(display, appWin->win, CWBorderWidth, &xwc);
		}

		XReparentWindow(display, appWin->win, sandboxWin,
			rBORDER_WIDTH, rBORDER_WIDTH);
	}

	if(appWin->mode == MAIN_APP) {
		removeFromAppList(&mainAppList, appWin);
		XFree(appWin->gc);

		XReparentWindow(display, appWin->parent, sandboxWin, 0, 0);
		XDestroyWindow(display, appWin->parent);
		reFocus();
	}
	else if(appWin->mode == APPLET) {
		removeFromAppList(&appletList, appWin);
		reorderApplets();	

		if(! shutdownStarted) {
			Atom actual_type_return;
			int actual_format_return;
			unsigned long nitems_return;
			unsigned long bytes_after_return;
			long *data;
			int status;
			Atom appletTag;

			appletTag = XInternAtom(display, WM_APPLET_TAG, True);
			if(touchWin && appletTag != None) {
				status = XGetWindowProperty(display, appWin->win, appletTag,
					0, 1, False, XA_INTEGER,
					&actual_type_return, &actual_format_return,
					&nitems_return, &bytes_after_return,
					(unsigned char **) &data);

				if(status == Success) {
					XDeleteProperty(display, appWin->win, appletTag);
				}
			}
		}
	}
	else {
		tickerMsg("Attempting to unassign weird window");
	}

	appWin->mode = UNASSIGNED;

	appWin->parent = None;
	appWin->gc = NULL;
}

//---------------------------------------------------------
// Reparent Main App window with a border window
void makeMainApp(AppWin *appWin) {
	int screen;
	XFontStruct *fontInfo;
	int textHeight;
	XWindowChanges xwc;
	XGCValues values;
	Font fontId;

	if(rVERBOSE) tickerMsg("Reparenting Main App\n");
	if(appWin == NULL || appWin->mode == MAIN_APP)
		return;

	if(appWin->mode != UNASSIGNED)
		unassignWin(appWin, True);

	screen = getScreen(appWin->root);
	if(screen == -1) {
		fprintf(stderr, "Error finding screen number for window\n");
		exit(1);
	}

	appWin->gc = sandboxGC;

	fontId = XLoadFont(display, rFONT_NAME);
	fontInfo = XQueryFont(display, fontId);
	textHeight = fontInfo->ascent+fontInfo->descent;

	// printf("Reparent x: %d y: %d width: %d height: %d\n",
	 // appWin->x, appWin->y, appWin->width, appWin->height);
	appWin->parent = XCreateSimpleWindow(display, sandboxWin, 
		appWin->x, appWin->y, 
		appWin->width+2*rBORDER_WIDTH, appWin->height+3*rBORDER_WIDTH+textHeight, 
		0, WhitePixel(display, screen), BlackPixel(display, screen));
	XReparentWindow(display, appWin->parent, appWin->root, appWin->x, appWin->y);
	XSelectInput(display, appWin->parent, 
		ExposureMask | SubstructureNotifyMask | SubstructureRedirectMask | PropertyChangeMask);

	appWin->gc = XCreateGC(display, appWin->parent, 0, &values);
	XSetFont(display, appWin->gc, fontId);

	if(! appWin->inputOnly) {
		xwc.border_width = 0;
		XConfigureWindow(display, appWin->win, CWBorderWidth, &xwc);
	}

	XReparentWindow(display, appWin->win, appWin->parent,
		rBORDER_WIDTH, 2*rBORDER_WIDTH+textHeight);
	if(appWin->isMapped) {
		XMapWindow(display, appWin->parent);
		XMapWindow(display, appWin->win);

		if(! appWin->isSaved ) {
			XAddToSaveSet(display, appWin->win);
			appWin->isSaved = 1;
		}
	}

	XSetWindowBackground(display, appWin->parent, screens[screen].inactive);
	XClearWindow(display, appWin->parent);

	appWin->mode = MAIN_APP;
	addToAppList(&mainAppList, appWin);

	winMain(appWin);

	reFocus();
}


//---------------------------------------------------------
// Reparent applet in applet container window
void makeApplet(AppWin *appWin) {
	XWindowChanges xwc;
	Atom atomId;
	long state;

	if(rVERBOSE) tickerMsg("Reparenting Applet\n");
	if(appWin == NULL || appWin->mode == APPLET)
		return;

	if(appWin->mode != UNASSIGNED)
		unassignWin(appWin, True);

	appWin->gc = appletGC;

	// printf("Reparent x: %d y: %d width: %d height: %d\n",
	 // appWin->x, appWin->y, appWin->width, appWin->height);
	XSelectInput(display, appWin->win, PropertyChangeMask);

	if(! appWin->inputOnly) {
		xwc.border_width = rAPPLET_BORDER;
		XConfigureWindow(display, appWin->win, CWBorderWidth, &xwc);
	}

	appWin->parent = appletWin;
	XReparentWindow(display, appWin->win, appWin->parent,
		rBORDER_WIDTH, rBORDER_WIDTH);

	if(appWin->isMapped) {
		XMapWindow(display, appWin->win);

		if(! appWin->isSaved ) {
			XAddToSaveSet(display, appWin->win);
			appWin->isSaved = 1;
		}
	}

	state = 1;
	atomId = XInternAtom(display, WM_APPLET_TAG, False);
	XChangeProperty(display, appWin->win, atomId, XA_INTEGER, 32, PropModeReplace, (unsigned char*) &state, 1);

	appWin->mode = APPLET;
	addToAppList(&appletList, appWin);

	winApplet(appWin);
	reorderApplets();
}

//---------------------------------------------------------
// Classify window as Main App or Applet and reparent accordingly
void classifyWin(AppWin *appWin) {
	Atom actual_type_return;
	int actual_format_return;
	unsigned long nitems_return;
	unsigned long bytes_after_return;
	long *data;
	int status;
	Atom appletTag;
	Window prop = None;
	char buffer[128];

	if(rVERBOSE) {
		sprintf(buffer, "Classifying %s", appWin->name);
		tickerMsg(buffer);
	}

	appletTag = XInternAtom(display, WM_APPLET_TAG, True);
	if(appletTag != None) {
		status = XGetWindowProperty(display, appWin->win, appletTag, 
			0, 1, False, XA_INTEGER, 
			&actual_type_return, &actual_format_return, 
			&nitems_return, &bytes_after_return,
			(unsigned char **) &data);

		if(status == Success) {
			if(actual_type_return == XA_INTEGER)
				makeApplet(appWin);
			else
				makeMainApp(appWin);

			XFree(data);
		}
		else {
			makeMainApp(appWin);
		}
	}
	else {

		if(rTRANSIENTS_TO_APPLETS && XGetTransientForHint(display, appWin->win, &prop))
			makeApplet(appWin);
		else
			makeMainApp(appWin);
	}
}

//---------------------------------------------------------
// draw in title bar for Main Apps
void drawTitle(AppWin *appWin) {
	char dataStr[256];
	XFontStruct *fontInfo;
	GContext gContext;
	int screen = 0;
	XColor color, exact;

	if(appWin == NULL || appWin->mode != MAIN_APP)
		return;

	if(appWin == mainAppList)
		XAllocNamedColor(display, DefaultColormap(display, screen), rACTIVE_TITLE_COLOR, &color, &exact);
	else
		XAllocNamedColor(display, DefaultColormap(display, screen), rINACTIVE_TITLE_COLOR, &color, &exact);
	XSetForeground(display, appWin->gc, color.pixel);
	gContext = XGContextFromGC(appWin->gc);
	fontInfo = XQueryFont(display, gContext);

	if(appWin->name == NULL)
		strcpy(dataStr, "Unknown");
	else
		strcpy(dataStr, appWin->name);

	XClearWindow(display, appWin->parent);
	XDrawString(display, appWin->parent, appWin->gc,
		rBORDER_WIDTH, rBORDER_WIDTH+fontInfo->ascent, 
		dataStr, strlen(dataStr));
}

//---------------------------------------------------------
void winMain(AppWin *appWin) {
	XWindowChanges xwc;
	XFontStruct *fontInfo;
	int textHeight;
	GContext gContext;

	if(appWin == NULL || appWin->mode != MAIN_APP)
		return;

	gContext = XGContextFromGC(appWin->gc);
	fontInfo = XQueryFont(display, gContext);
	textHeight = fontInfo->ascent+fontInfo->descent;

	appWin->actualWidth = windowWidth-2*rBORDER_WIDTH;
	appWin->actualHeight = windowHeight-3*rBORDER_WIDTH-textHeight;

	// printf("Main Win %s %dx%d\n", appWin->name, appWin->actualWidth, appWin->actualHeight);

	xwc.x = rBORDER_WIDTH;
	xwc.y = 2*rBORDER_WIDTH+textHeight;
	xwc.width = appWin->actualWidth;
	xwc.height = appWin->actualHeight;
	XConfigureWindow(display, appWin->win, 
		CWX | CWY | CWWidth | CWHeight, &xwc);

	xwc.x = rWINDOW_OFFSET;
	xwc.y = rWINDOW_OFFSET;
	xwc.width = windowWidth;
	xwc.height = windowHeight;
	XConfigureWindow(display, appWin->parent, 
		CWX | CWY | CWWidth | CWHeight, &xwc);
}

//---------------------------------------------------------
void winApplet(AppWin *appWin) {
	XWindowChanges xwc;

	if(appWin == NULL || appWin->mode != APPLET)
		return;

	appWin->actualWidth = appletWidth-2*rBORDER_WIDTH;
	appWin->actualHeight = rAPPLET_HEIGHT;

	// printf("Applet Win %s %dx%d\n", appWin->name, appWin->actualWidth, appWin->actualHeight);
	
	xwc.x = rBORDER_WIDTH;
	xwc.y = rBORDER_WIDTH;
	xwc.width = appWin->actualWidth;
	xwc.height = appWin->actualHeight;
	XConfigureWindow(display, appWin->win, 
		CWX | CWY | CWWidth | CWHeight, &xwc);
}

//---------------------------------------------------------
void winFull(AppWin *appWin) {
	XWindowChanges xwc;

	if(appWin == NULL)
		return;

	appWin->actualWidth = rootWidth;
	appWin->actualHeight = rootHeight;

	// printf("Full Win %s %dx%d\n", appWin->name, appWin->actualWidth, appWin->actualHeight);
	
	xwc.x = 0;
	xwc.y = 0;
	xwc.width = appWin->actualWidth;
	xwc.height = appWin->actualHeight;
	XConfigureWindow(display, appWin->win, 
		CWX | CWY | CWWidth | CWHeight, &xwc);

	if(appWin->mode == MAIN_APP) {
		xwc.width = appWin->actualWidth;
		xwc.height = appWin->actualHeight;
		XConfigureWindow(display, appWin->parent, 
			CWX | CWY | CWWidth | CWHeight, &xwc);
	}
}

//---------------------------------------------------------
void setAppletPos(AppWin *applet, int pos) {
	XWindowChanges xwc;
	char buffer[256];

	if(applet == NULL || applet->mode != APPLET || ! applet->isMapped)
		return;

	if(rVERBOSE) {
		sprintf(buffer, "Set Applet Pos - x: %d y: %d", xwc.x, xwc.y);
		tickerMsg(buffer);
	}

	xwc.x = rBORDER_WIDTH;
	xwc.y = rBORDER_WIDTH+pos*(rAPPLET_HEIGHT+rAPPLET_SPACING);
	XConfigureWindow(display, applet->win, CWX | CWY, &xwc);
}

