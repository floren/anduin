/*
 * commands.c
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Handle all the input commands that can ge given to Anduin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/keysym.h>

#include "anduin.h"
#include "resources.h"

void startApp(char *app) {
	pid_t child;
	char buffer[256];

	if(app == NULL)
		return;

	sprintf(buffer, "Start App '%s'", app);
	tickerMsg(buffer);

	if(rVERBOSE) tickerMsg("Start App");
	child = fork();
	if(child == 0) {
		close(ConnectionNumber(display));
		execlp(app, app, 0);
		exit(1);
	}
	else if(child == -1) {
		tickerMsg("Error opening app");
	}
}

void newTerm() {
	pid_t child;

	if(rVERBOSE) tickerMsg("New Xterm");
	child = fork();
	if(child == 0) {
		close(ConnectionNumber(display));
		execlp(xtermProgName, xtermProgName, "-fn", rFONT_NAME, 0);
		exit(1);
	}
	else if(child == -1) {
		tickerMsg("Error opening xterm");
	}
}

void killWin() {
	char buffer[256];

	if(mainAppList != NULL) {
		if(rVERBOSE)  {
			if(mainAppList->name != NULL)
			  sprintf(buffer, "Window: %s Killed", mainAppList->name);
			else
			  strcpy(buffer, "Window: Unknown Killed");
			tickerMsg(buffer);
		}

		removeWin(mainAppList, True);
	}
}

void fullsize() {
	winFull(mainAppList);
}

void stdsize() {
	winMain(mainAppList);
}

//------------------------------------------------------------

void doStrCommand(char *str) {
	KeyBind *keyBind;

	keyBind = findKeyEntryByCmd(strToSymbol(str));
	doCommand( keyBind->cmd, keyBind->arg);
}

//------------------------------------------------------------

void doKeyCommand(KeySym keySym, unsigned int mask) {
	KeyBind *ptr;

	ptr = findKeyEntry(keySym, mask);

	if(ptr == NULL) {
		// printf("Command '%s' Not Found\n", XKeysymToString(keySym));
		return;
	}

	if(ptr->cmd == CMD_MODE) {
		// printf("Invoking Mode\n");
		switchCmdMode(ptr);
	}
	else {
		doCommand(ptr->cmd, ptr->arg);
	}
}

//------------------------------------------------------------

void selectMainApp(int num) {
	AppWin *ptr = mainAppList;
	int i;

	for(i=0; i<num && ptr != NULL; i++) {
		ptr = ptr->next;
	}

	if(ptr != NULL)
		setActiveWin(ptr);
}

//------------------------------------------------------------

void selectApplet(int num) {
	AppWin *ptr = appletList;
	int i;

	for(i=0; i<num && ptr != NULL; i++) {
		ptr = ptr->next;
	}

	if(ptr != NULL)
		makeMainApp(ptr);
}

//------------------------------------------------------------

void doCommand(int cmd, char *arg) {
	switch(cmd) {
		case CMD_NEW_TERM: 
			newTerm();
			break;
		case CMD_KILL_WIN: 
			killWin();
			break;
		case CMD_SHUTDOWN: 
			shutdownWM();
			break;
		case CMD_FULL_SIZE: 
			fullsize();
			break;
		case CMD_STD_SIZE: 
			stdsize();
			break;
		case CMD_MAKE_APPLET: 
			makeApplet(mainAppList);
			break;
		case CMD_CHATTER: 
			if(arg == NULL) {
				tickerMsg("");
				fprintf(stderr, "\n");
			}
			else {
				tickerMsg(arg);
				fprintf(stderr, "%s\n", arg);
			}
			break;
		case CMD_START_APP:
			if(arg != NULL)
				startApp(arg);
			break;
		case CMD_MAIN_APP:
			if(arg != NULL)
				selectMainApp(atoi(arg));
			else
				selectMainApp(1);
			break;
		case CMD_APPLET:
			if(arg != NULL)
				selectApplet(atoi(arg));
			else
				selectApplet(0);
			break;
		case CMD_DEBUG:
			printDebug();
			break;
		case CMD_NEXT_APP:
			shiftNext();
			break;
		case CMD_PREV_APP:
			shiftPrev();
			break;
		case CMD_NONE:
			break;
		default:
			tickerMsg("Unknown Command");
			break;
	};
}

