/*
 * anduin.h
 * Author: Steve Dunn, BorgLab MIT 
 * copyright (C) 2001 MIT Media Lab
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>

#include "resources.h"

// #define USE_CONTEXT // Compile in enchant_ipc libraries

#define WM_APPLET_TAG "AND_applet_tag"
#define WM_TRANSIENT_TAG "AND_transient_tag"
#define WM_CONTEXT_CHANGE "AND_enchant_event"
#define WM_ENCHANT_PRIORITY "AND_enchant_priority"

#define XTERM_COMMAND "xterm"

#define CONFIG_FILE "config.anduin"

// Modes
#define UNASSIGNED 0
#define MAIN_APP 1
#define APPLET 2
#define OVERRIDE 3

// Resource Types
#define RESOURCE_STRING 1
#define RESOURCE_INT 2
#define RESOURCE_BOOLEAN 3
#define RESOURCE_FLOAT 4

//------------------------------------

// Window Manager representation of each client window
typedef struct _AppWin {
	Window win;
	Window parent;
	Window root;
	int screen;
	GC gc;
	int x;
	int y;
	int width;
	int height;
	int actualWidth;
	int actualHeight;
	int isMapped;
	int isSaved;
	int mode;
	int priority;
	int isTransient;

	int focusNotify;
	int deleteNotify;
	int saveNotify;

	int inputOnly;

	time_t timestamp;

	char *name;

	struct _AppWin *nextApp;
	struct _AppWin *next;
} AppWin;

// Window Manager representation of each screen in X
typedef struct {
	int screen;
	Window root;

	unsigned long active;
	unsigned long inactive;

	unsigned long appletBG;
	unsigned long borderColor;
} ScreenData;

// Structure for specifying user configurable resource variables
typedef struct {
	char *name;
	void *ptr;
	int type;
	char *defaultVal;
} Resource;

typedef struct {
	int defaultKeySym;
	unsigned int defaultMask;
	int cmd;
	char *str;
} Command;

typedef struct _KeyBind {
	KeySym keySym;
	int cmd;
	unsigned int mask;
	char *arg;

	struct _KeyBind *keys;
	struct _KeyBind *parent;
	struct _KeyBind *next;
} KeyBind;

//------------------------------------
extern int numResources;
extern Resource resourceList[];

extern int numCmds;
extern Command commandList[];

//------------------------------------

extern Display *display; // global X windows display var
extern int numScreens; // num X windows screens
extern ScreenData *screens; // list of X windows screens
extern char *xtermProgName; // shell command to launch Xterm
extern int shutdownStarted; // Window Manager in the process of shutting down

extern Window sandboxWin; // temporary window to create new windows without generating root createWindow events
extern GC sandboxGC; // GC for sandboxWin and template for all parent windows
extern Window appletWin; // applet parent window
extern GC appletGC; // applet parent window graphics context
extern Window taskWin; // task list window
extern GC taskGC; // task list graphics context
extern Window tickerWin; // ticker window
extern GC tickerGC; // ticker Graphics context

extern int rootHeight;
extern int rootWidth;
extern int appletWidth;
extern int taskWidth;
extern int windowHeight;
extern int windowWidth;
extern Atom contextChangeAtom; // atom id for the context property change used by the context.c thread
extern int usingContextServer;

extern AppWin *mainAppList; // list of main apps
extern AppWin *appletList; // list of applets

//------------------------------------

// anduin.c
int getScreen(Window root);
void shutdownWM();

// ticker.h
void initTicker();
void tickerMsg(char *str);
void drawTicker();
void drawTickerMsg(char *str);

// modes.c
void classifyWin(AppWin *appWin);
void makeMainApp(AppWin *appWin);
void makeApplet(AppWin *appWin);
void winFull(AppWin *appWin);
void winMain(AppWin *appWin);
void winApplet(AppWin *appWin);
void drawTitle(AppWin *appWin);
void setAppletPos(AppWin *applet, int pos);
void unassignWin(AppWin *appWin, int touchWin);

// manage.c
AppWin* addWin(Window win);
AppWin* getWin(Window win);
AppWin* getWinFromParent(Window win);
void removeWin(AppWin *appWin, int destroyClient);
void showAppList();
void setActiveWin(AppWin *appWin);
void nextWin();
void reFocus();
void removeAll();
void reorderApplets();
void addToAppList(AppWin **list, AppWin *appWin);
void removeFromAppList(AppWin **list, AppWin *appWin);
void printDebug();

// events.c
void dispatch(XEvent *event);
int getWinName(AppWin *appWin);
int getWinProtocols(AppWin *appWin);
int getWinPriority(AppWin *appWin);
int getWinAppletTag(AppWin *appWin);
int getWinTransientTag(AppWin *appWin);

// context.c
int contextInit();
void contextClose();
void contextListen();
void readContext();
void sendContextString(char *unl, char *str);
int contextChanged();

// resources.c
int fileExists(char *filename);
void parseResources(Resource *resources, int numResources, char *filename);
void dumpResources(Resource *resources, int numResources);

// keybind.c
int keySymToSymbol(KeySym keySym);
int strToSymbol(char *str);
char* symbolToStr(int symbol);
void parseKeys(char *filename);
void dumpAllKeys();
void dumpKeys(KeyBind *list, int level);
int findKey(KeySym keySym, unsigned int mask);
KeyBind* findKeyEntry(KeySym keySym, unsigned int mask);
KeyBind* findKeyEntryByCmd(int cmd);
void grabKeys(Window win);
void ungrabKeys(Window win);
void releaseKey(KeySym keySym, unsigned int mask);
void switchCmdMode(KeyBind *newMode);

// commands.c
void doKeyCommand(KeySym keySym, unsigned int mask);
void doStrCommand(char *str);
void doCommand(int cmd, char *arg);

