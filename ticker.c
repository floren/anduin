/*
 * ticker.c
 *
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Functions for handling the ticker message box, used for displaying messages to the system
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/Xatom.h>
#include <X11/Xproto.h>

#include "anduin.h"

char** tickerData;
int tickerPtr;

//---------------------------------------------------------
// Initialize the Ticker data

void initTicker() {
	int i;

	tickerData = (char**) malloc(rNUM_TICKER_LINES*sizeof(char**));
	for(i=0; i<rNUM_TICKER_LINES; i++) {
		tickerData[i] = (char*) malloc((1+rMAX_TICKER_LENGTH)*sizeof(char));

		if(tickerData[i] == NULL) {
			fprintf(stderr, "Failed to allocate ticker buffers of length %d\n", rMAX_TICKER_LENGTH);
			exit(1);
		}
		tickerData[i][0] = '\0';
		tickerData[i][rMAX_TICKER_LENGTH] = '\0';
	}
	tickerPtr = 0;
}

//---------------------------------------------------------
// Draw text message to ticker window
void drawTicker() {
	XFontStruct *fontInfo;
	int textHeight;
	GContext gContext;
	int i, ptr;

	if(tickerGC == NULL) {
		return;
	}

	gContext = XGContextFromGC(tickerGC);
	fontInfo = XQueryFont(display, gContext);
	textHeight = fontInfo->ascent+fontInfo->descent;

	XClearWindow(display, tickerWin);

	i = 0;
	for(ptr = tickerPtr; ptr < rNUM_TICKER_LINES; ptr++) {
		XDrawString(display, tickerWin, tickerGC,
			rBORDER_WIDTH, rBORDER_WIDTH+i*textHeight+fontInfo->ascent,
			tickerData[ptr], strlen(tickerData[ptr]));
		i++;
	}
	for(ptr = 0; ptr < tickerPtr; ptr++) {
		XDrawString(display, tickerWin, tickerGC,
			rBORDER_WIDTH, rBORDER_WIDTH+i*textHeight+fontInfo->ascent,
			tickerData[ptr], strlen(tickerData[ptr]));
		i++;
	}
}

//---------------------------------------------------------
// Draw text message to ticker window
void drawTickerMsg(char *str) {
	if(tickerGC == NULL) {
		printf("%s\n", str);
		return;
	}

	strncpy(tickerData[tickerPtr], str, rMAX_TICKER_LENGTH);

	if(rTICKER_TEXT_OUTPUT)
		printf("%s\n", tickerData[tickerPtr]);

	tickerPtr++;
	if(tickerPtr >= rNUM_TICKER_LINES)
		tickerPtr = 0;

	drawTicker();
}

//---------------------------------------------------------
// Send text message to ticker window
void tickerMsg(char *str) {

	if(usingContextServer) 
		sendContextString("/Anduin/TickerMsg", str);
	else
		drawTickerMsg(str);
}

