# Anduin Makefile
# Author: Steve Dunn, BorgLab MIT copyright 2001

# gcc -o anduin anduin.c -I/usr/X11R6/include/X11 -L/usr/X11R6/lib -lX11

GCC = gcc
CFLAGS = -g -Wall -I../enchant_ipc/include -I/usr/X11R6/include
# -DUSE_CONTEXT
LDFLAGS = -L/usr/X11R6/lib -lX11 -lpthread
# -L../enchant_ipc/lib -lenchant_ipc_client
# -lefence

PROGS = anduin keyview
ANDUIN_OBJS = anduin.o manage.o events.o modes.o context.o resources.o resourceList.o commands.o ticker.o keybind.o
KEYVIEW_OBJS = keyview.o resourceList.o resources.o keybind.o

all: $(PROGS)
	cd Tests; make

anduin.o: anduin.c anduin.h resources.h
ticker.o: ticker.c anduin.h resources.h
manage.o: manage.c anduin.h resources.h
events.o: events.c anduin.h resources.h
modes.o: modes.c anduin.h resources.h
context.o: context.c anduin.h resources.h
resources.o: resources.c anduin.h resources.h
commands.o: commands.c anduin.h resources.h
keybind.o: keybind.c anduin.h resources.h

anduin: $(ANDUIN_OBJS)
	$(GCC) -o $@ $(ANDUIN_OBJS) $(LDFLAGS)

keyview: $(KEYVIEW_OBJS)
	$(GCC) -o $@ $(KEYVIEW_OBJS) $(LDFLAGS)

clean:
	rm -f *.o core $(PROGS)
	cd Tests; make clean

