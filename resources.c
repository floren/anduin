/*
 * resources.c
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Retrieve Window Manager configuration from the X windows resource manager
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "anduin.h"

#define MAX_BUF_SIZE 256

void updateResource(Resource *resources, int numResources, char *name, char *value) {
	int i;

	for(i=0; i<numResources; i++) {
		if(strcmp(name, resources[i].name) == 0) {
			if(resources[i].type == RESOURCE_STRING) {
				char *tmp;

				// printf("Match for string resource '%s'\n", name);
				tmp = *((char**) resources[i].ptr);
				if( tmp != NULL)
					free(tmp);

				tmp = malloc(strlen(value)+1);
				strcpy(tmp, value);
				*((char**) resources[i].ptr) = tmp;
			}
			else if(resources[i].type == RESOURCE_INT) {
				// printf("Match for int resource '%s'\n", name);
				*((int*) resources[i].ptr) = atoi(value);
			}
			else if(resources[i].type == RESOURCE_FLOAT) {
				// printf("Match for float resource '%s'\n", name);
				*((float*) resources[i].ptr) = atof(value);
			}
			else if(resources[i].type == RESOURCE_BOOLEAN) {
				// printf("Match for boolean resource '%s'\n", name);
				if(strcmp(value, "TRUE") == 0)
					*((int*) resources[i].ptr) = 1;
				else if(strcmp(value, "FALSE") == 0)
					*((int*) resources[i].ptr) = 0;
				else
					printf("Error - invalid boolean value for %s\n", name);
			}
			else {
				printf("Error - unkown type for '%s'\n", name);
			}

			break;
		}
	}
}

void initResources(Resource *resources, int numResources) {
	int i;

	for(i=0; i<numResources; i++) {
		if(resources[i].defaultVal != NULL)
			updateResource(resources, numResources, resources[i].name, resources[i].defaultVal);
		else
			updateResource(resources, numResources, resources[i].name, "");
	}
}

void dumpResources(Resource *resources, int numResources) {
	int i;

	for(i=0; i<numResources; i++) {
		if(resources[i].type == RESOURCE_STRING)
			printf("String %s: '%s'\n", resources[i].name, *((char**) resources[i].ptr));
		else if(resources[i].type == RESOURCE_INT)
			printf("Int %s: %d\n", resources[i].name, *((int*) resources[i].ptr));
		else if(resources[i].type == RESOURCE_BOOLEAN)
			printf("Boolean %s: %d\n", resources[i].name, *((int*) resources[i].ptr));
	}
}

int fileExists(char *filename) {
	struct stat stats;

	if( stat(filename, &stats) == 0 ) {
		return 1;
	}
	else {
		return 0;
	}
}

void parseResources(Resource *resources, int numResources, char *filename) {
	FILE *fp;
	char buffer[MAX_BUF_SIZE+1];
	char *ptr;
	int useHomeDir = 0;

	if( filename == NULL ) {
		sprintf(buffer, "%s/.%s", getenv("HOME"), CONFIG_FILE);
		if( fileExists(buffer)) {
			filename = buffer;
			useHomeDir = 1;
		}
		else if(fileExists(CONFIG_FILE)) {
			filename = CONFIG_FILE;
		}
	}
printf("Reading config file: %s\n", filename);

	initResources(resources, numResources);

	if(filename == NULL)
		return;

	fp = fopen(filename, "r");
	if(fp == NULL) {
		fprintf(stderr, "Error reading config file '%s'\n", filename);
		return;
	}
	
	while( fgets(buffer, MAX_BUF_SIZE, fp) != NULL) {
		if(*buffer == '#')
			continue;

		ptr = strchr(buffer, '\n');
		if(ptr != NULL)
			*ptr = '\0';

		ptr = strchr(buffer, '=');
		if(ptr != NULL) {
			*ptr = '\0';
			updateResource(resources, numResources, buffer, ptr+1);
		}
		else {
			printf("Bad Entry: %s", buffer);
		}
	}

	fclose(fp);

	filename = rKEY_FILE;
	if( strchr(rKEY_FILE, '/') == NULL && useHomeDir) {
		sprintf(buffer, "%s/%s", getenv("HOME"), rKEY_FILE);
		filename = buffer;
	}

	parseKeys(filename);
}

