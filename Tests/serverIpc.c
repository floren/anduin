/*
 * serverIpc.c
 *
 * Context Manager anduin simulator
 *
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Handle all the input commands that can ge given to Anduin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "client_ipc.h"

static int messageWaiting = 0;

void tickerMsg(char *msg) {
	printf("%s\n", msg);
}

int contextInit() {
	NodeType *node, *branch;
	char buffer[256];
	DataType data;

	printf("Connecting to Context Server\n");
	strcpy(buffer, "Anduin Window Manager v0.1");
	data.string_val = buffer;
	node = CreateNode("Anduin", T_STRING, strlen(buffer), &data, FALSE);
	InitClientIPC(node);
	DeleteNode(NULL, node);

	branch = CreateBranchNode("Anduin", T_BRANCH, 0);
	
	strcpy(buffer, "Anduin Started");
	data.string_val = buffer;
	node = CreateNode("TickerMessage", T_STRING, strlen(buffer), &data, FALSE);
	InsertNode(branch, node);

	if(PublishNode("18.85.44.216:/Anduin", branch, NULL, TRUE) == FALSE && 0) {
		printf("Could not connect to context server\n");
		return 0;
	}
	else {
		printf("First Node published\n");

		// LockNode("18.85.44.216:/Anduin", FALSE, TRUE);

		SubscribeNode("18.85.44.216:/Anduin/TickerMessage", TRUE, TRUE);
		return 1;
	}
}

void readContext() {
	NodeType *senderNode, *mesgNode, *updateNode, *respNode;
	char *unl, *publisher;
	char buffer[256];
	char *callType, *returnUnl, *result;

	// tickerMsg("Read Context");
	if(DequeueMessage(&senderNode, &mesgNode, FALSE) && senderNode != NULL && mesgNode != NULL) {
		tickerMsg("Got Message");
		DeleteNode(NULL, senderNode);
		DeleteNode(NULL, mesgNode);
	}

	if(DequeueUpdate(&unl, &publisher, &updateNode, FALSE) && unl != NULL && updateNode != NULL) {
		// sprintf(buffer, "Got update to '%s'", unl);
		// tickerMsg(buffer);

		if(strcmp(updateNode->name, "TickerMessage") == 0) {
			// tickerMsg("Got New Ticker Message");

			if(IsType(updateNode->type, T_STRING)) {
				sprintf(buffer, "INCOMING '%s'", updateNode->data.string_val);
				tickerMsg(buffer);
			}
			else {
				tickerMsg("INCOMING not a string");
			}
		}
		DeleteNode(NULL, updateNode);
	}

	if(DequeueResponse(&callType, &returnUnl, &respNode, &result, FALSE) == TRUE && result != NULL) {
		tickerMsg("Got Response");
		DeleteNode(NULL, respNode);
	}

	messageWaiting = 0;
}

void contextClose() {
	ShutDownClientIPC();
}

int main(int argc, char *argv[]) {
	contextInit();

	while(1) {
		// printf("Context Event\n");
printf("Start WaitForNodes\n");
			WaitForNodes();
printf("End WaitForNodes\n");

		readContext();
	}

	contextClose();
	return 0;
}

