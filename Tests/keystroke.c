/*
 * keystroke.c
 *
 * A minimal X Windows Test program 
 *
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Handle all the input commands that can ge given to Anduin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdlib.h>
#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>

int main(int argc, char *argv[]) {
	Display *display;
	int screenNum;
	Window win;
	XEvent event;
	GC gc;
	XGCValues values;
	char buffer[256];

	XKeyEvent *keyEvent;
	int numChars;
	int bufSize = 20;
	KeySym keysym;
	XComposeStatus compose;
    int textHeight;
    XFontStruct *fontInfo;
    GContext gContext;

	if( (display=XOpenDisplay("")) == NULL) {
		fprintf(stderr, "Cannot open display\n");
		exit(1);
	}

	screenNum = DefaultScreen(display);
	win = XCreateSimpleWindow(display, RootWindow(display, screenNum), 
		100, 200, 600,200, 1, 
		BlackPixel(display, screenNum), WhitePixel(display, screenNum));
	gc = XCreateGC(display, win, 0, &values);

	XSelectInput(display, win, ExposureMask | KeyPressMask);

	XMapWindow(display, win);

	strcpy(buffer, "KeyStroke");

    gContext = XGContextFromGC(gc);
    fontInfo = XQueryFont(display, gContext);
    textHeight = fontInfo->ascent+fontInfo->descent;

	while(1) {
		XNextEvent(display, &event);
		switch(event.type) {
			case Expose:
				// printf("Expose event\n");
				XDrawString(display, win, gc, 10, fontInfo->ascent, buffer, strlen(buffer));
				break;
			case KeyPress:
				keyEvent = &event.xkey;

				numChars = XLookupString(keyEvent, buffer, bufSize, &keysym,
					&compose);

				XClearWindow(display, win);
				sprintf(buffer, "KeyPress - code: 0x%x", keyEvent->keycode);
				XDrawString(display, win, gc, 10, fontInfo->ascent, buffer, strlen(buffer));

				sprintf(buffer, "modifiers: 0x%x Keysym 0x%lx", keyEvent->state, keysym);
				XDrawString(display, win, gc, 10, fontInfo->ascent+textHeight, buffer, strlen(buffer));

				sprintf(buffer, "Char '%s'", XKeysymToString(keysym));
				XDrawString(display, win, gc, 10, fontInfo->ascent+2*textHeight, buffer, strlen(buffer));

				break;
			default:
				// printf("Unknown event\n");
				break;
		}
	}

	XCloseDisplay(display);

	return 0;
}

