/*
 * applet.c
 *
 * A minimal X Windows Test program 
 *
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Handle all the input commands that can ge given to Anduin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

int main(int argc, char *argv[]) {
	Display *display;
	int screenNum;
	Window win;
	XEvent event;
	GC gc;
	XGCValues values;
	char buffer[256];
	Atom atomId;
	long state;

	if( (display=XOpenDisplay("")) == NULL) {
		fprintf(stderr, "Cannot open display\n");
		exit(1);
	}

	screenNum = DefaultScreen(display);
	win = XCreateSimpleWindow(display, RootWindow(display, screenNum), 
		100, 200, 200,200, 1, 
		BlackPixel(display, screenNum), WhitePixel(display, screenNum));

	XStoreName(display, win, "Applet Test");

	state=1;
	atomId = XInternAtom(display, "AND_applet_tag", False);
	XChangeProperty(display, win, atomId, XA_INTEGER, 32, PropModeReplace, (unsigned char*) &state, 1);

	state=1;
	atomId = XInternAtom(display, "AND_enchant_priority", False);
	XChangeProperty(display, win, atomId, XA_INTEGER, 32, PropModeReplace, (unsigned char*) &state, 1);

	XSelectInput(display, win, ExposureMask);
	gc = XCreateGC(display, win, 0, &values);
	XMapWindow(display, win);

	strcpy(buffer, "Applet");
	while(1) {
		XNextEvent(display, &event);
		switch(event.type) {
			case Expose:
				// printf("Expose event\n");
				XDrawString(display, win, gc, 10, 20, buffer, strlen(buffer));
				break;
			default:
				printf("Unknown event\n");
				break;
		}
	}

	XCloseDisplay(display);

	return 0;
}

