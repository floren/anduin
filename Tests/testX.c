/*
 * textX.c
 *
 * A test application for X Windows
 *
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Handle all the input commands that can ge given to Anduin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdlib.h>
#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>

int main(int argc, char *argv[]) {
	Display *display;
	char *displayName = NULL;
	int screenNum;
	Window win;
	XEvent event;
	Pixmap icon;
	char buffer[256];
	GC gc;
    unsigned long valuemask = 0; /* ignore XGCvalues and use defaults */
    XGCValues values;
	
	int icon_width  = 40;
	int icon_height  = 40;
	static unsigned char icon_bits[] = {
	   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc0,
	   0xff, 0x01, 0x00, 0x00, 0x30, 0x00, 0x06, 0x00, 0x00, 0x0c, 0x00, 0x18,
	   0x00, 0x00, 0x03, 0x00, 0x60, 0x00, 0x80, 0x00, 0x00, 0x80, 0x00, 0x40,
	   0x00, 0x00, 0x00, 0x01, 0x20, 0x00, 0x00, 0x00, 0x02, 0x20, 0x00, 0x00,
	   0x00, 0x02, 0x10, 0x00, 0x00, 0x00, 0x04, 0x10, 0x00, 0x00, 0x00, 0x04,
	   0x08, 0x00, 0x00, 0x00, 0x08, 0x08, 0x00, 0x00, 0x00, 0x08, 0x04, 0x00,
	   0x00, 0x00, 0x10, 0x04, 0x00, 0x00, 0x00, 0x10, 0x04, 0x00, 0x00, 0x00,
	   0x10, 0x04, 0x00, 0x00, 0x00, 0x10, 0x04, 0x00, 0x00, 0x00, 0x10, 0x04,
	   0x00, 0x00, 0x00, 0x10, 0x04, 0x00, 0x00, 0x00, 0x10, 0x04, 0x00, 0x00,
	   0x00, 0x10, 0x04, 0x00, 0x00, 0x00, 0x10, 0x04, 0x00, 0x00, 0x00, 0x10,
	   0x04, 0x00, 0x00, 0x00, 0x10, 0x08, 0x00, 0x00, 0x00, 0x08, 0x08, 0x00,
	   0x00, 0x00, 0x08, 0x10, 0x00, 0x00, 0x00, 0x04, 0x10, 0x00, 0x00, 0x00,
	   0x04, 0x20, 0x00, 0x00, 0x00, 0x02, 0x20, 0x00, 0x00, 0x00, 0x02, 0x40,
	   0x00, 0x00, 0x00, 0x01, 0x80, 0x00, 0x00, 0x80, 0x00, 0x00, 0x03, 0x00,
	   0x60, 0x00, 0x00, 0x0c, 0x00, 0x18, 0x00, 0x00, 0x30, 0x00, 0x06, 0x00,
	   0x00, 0xc0, 0xff, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

	char *prog_name = "testXapp";
	char *prog_class = "myapp";

	char *window_name = "Test X App";
	char *icon_name = "MyApp";
	XTextProperty windowName, iconName;
	XWMHints *wm_hints;
	XClassHint *class_hint;
	XSizeHints *size_hints;
	XColor color, exact;

	if( (display=XOpenDisplay(displayName)) == NULL) {
		fprintf(stderr, "Cannot open display\n");
		exit(1);
	}

	screenNum = DefaultScreen(display);

	win = XCreateSimpleWindow(display, RootWindow(display, screenNum), 100, 200, 200,200, 1, 
		BlackPixel(display, screenNum), WhitePixel(display, screenNum));

	wm_hints = XAllocWMHints();
	if(wm_hints == NULL) {
		fprintf(stderr, "Failure to allocate memory for wm hints\n");
		exit(1);
	}

	icon = XCreateBitmapFromData(display, win, icon_bits, icon_width, icon_height);
	wm_hints->initial_state = NormalState;
	wm_hints->input = True;
	wm_hints->icon_pixmap = icon;
	wm_hints->flags = StateHint | IconPixmapHint | InputHint;

	class_hint = XAllocClassHint();
	if(class_hint == NULL) {
		fprintf(stderr, "Failure to allocate memory for class hint\n");
		exit(1);
	}

	class_hint->res_name = prog_name;
	class_hint->res_class = prog_class;

	size_hints = XAllocSizeHints();
	if(size_hints == NULL) {
		fprintf(stderr, "Failure to allocate memory for size hints\n");
		exit(1);
	}

	size_hints->flags = PPosition | PSize | PMinSize;
	size_hints->min_width = 50;
	size_hints->min_height = 50;

	if(XStringListToTextProperty(&window_name, 1, &windowName) == 0) {
		fprintf(stderr, "structure allocation for windowName failed\n");
		exit(1);
	}

	if(XStringListToTextProperty(&icon_name, 1, &iconName) == 0) {
		fprintf(stderr, "structure allocation for iconName failed\n");
		exit(1);
	}

	XSetWMProperties(display, win, &windowName, &iconName, argv, argc, size_hints, wm_hints,
		class_hint);
	XMapWindow(display, win);

    XSelectInput(display, win, ExposureMask);

	strcpy(buffer, "This is a test.");
	gc = XCreateGC(display, win, valuemask, &values);
	XAllocNamedColor(display, DefaultColormap(display, screenNum), "light blue", &color, &exact);
	XSetWindowBackground(display, win, color.pixel);
	XClearWindow(display, win);

	while(1) {
		XNextEvent(display, &event);

		switch(event.type) {
			case Expose:
				// printf("Expose event\n");
				XDrawString(display, win, gc, 50, 50, buffer, strlen(buffer));

				break;
			default:
				printf("Unknown event\n");
				break;
		}
	}

	XCloseDisplay(display);

	return 0;
}

