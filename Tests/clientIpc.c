/*
 * clientIpc.c
 *
 * Context manager client test
 *
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Handle all the input commands that can ge given to Anduin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "client_ipc.h"

int main(int argc, char *argv[]) {
    NodeType *node;
    char buffer[256];
    DataType data;
	char *server = "dev-random";
	char *msg = "Client Talking";

	printf("Connecting to Context Server\n");
    strcpy(buffer, "Anduin Client");
    data.string_val = buffer;
    node = CreateNode("AnduinClient", T_STRING, strlen(buffer), &data, FALSE);
    InitClientIPC(node);
    DeleteNode(NULL, node);

    data.string_val = msg;
    node = CreateNode("TickerMessage", T_STRING, strlen(data.string_val), &data, FALSE);

	sprintf(buffer, "%s:/Anduin/TickerMessage", server);
    PublishNode(buffer, node, NULL, TRUE);
	printf("My Node published\n");

	ShutDownClientIPC();

	return 0;
}
