/*
 * anduin.c
 *
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Main Program 
 *  Anduin is a window manager designed as part
 *  of the Enchantment Interface of the MIThril
 *  wearable computing environment. 
 * http://www.media.mit.edu/wearables/
 * 
 * The main design contraints of this window manager
 *  are no pointing device, the user is only interacting
 *  with one application at a time, but is easily able
 *  to switch between multiple applications. In addition,
 *  it is possible to have multiple secondary applets
 *  providing the user with information. There is a 
 *  ticker/ticker window at the bottom that can be used
 *  by applications to provide data to the user and a 
 *  list of running applications visible to the user.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/Xatom.h>
#include <X11/Xproto.h>

#include "anduin.h"

Display *display;
int numScreens;
ScreenData *screens;
int rootWidth; 
int rootHeight;
int windowWidth; 
int windowHeight;
int appletWidth;
int taskWidth;

Window sandboxWin;
GC sandboxGC;
Window appletWin;
GC appletGC;
Window taskWin;
GC taskGC;
Window tickerWin;
GC tickerGC;
char *xtermProgName = XTERM_COMMAND;
int startingWM = 1; // state variable used on startup to detect if another window manager is already running

Atom contextChangeAtom;
int shutdownStarted = 0;

// Error handler with capability of dumping core for debugging
int errorHandler(Display *display, XErrorEvent *errorEvent) {
	char errorCode[128];
	char requestCode[64];
	char request[128];
	int *bomb = 0;

	if(errorEvent->request_code == X_GetWindowAttributes && errorEvent->error_code == BadWindow)
		return 0;

	if(errorEvent->request_code == X_ChangeWindowAttributes && errorEvent->error_code == BadAccess && startingWM) {
		fprintf(stderr, "Error - Another window manager is already running\n");
		exit(1);
	}

	XGetErrorText(display, errorEvent->error_code, errorCode, sizeof(errorCode));
	sprintf(requestCode, "%d", errorEvent->request_code);
	XGetErrorDatabaseText(display, "XRequest", requestCode, requestCode, request, sizeof(request));

	fprintf(stderr, "*** Error: %s - %s\n", request, errorCode);

	if(errorEvent->error_code == BadWindow ) {
		fprintf(stderr, "Error - Bad Window\n");
		return 1;
	}

	if(0) {
		*bomb = 1;
	}
	exit(1);

	return 0;
}

// Wait for exiting child process
void waitForChild() {
	wait(NULL);
}

// exit the window manager
void shutdownWM() {
	int screen;

	if(rVERBOSE) printf("Shutting Down\n");

	if(shutdownStarted) // MAJOR HACK - fiqure out and fix - sdunn
		return;
	shutdownStarted = 1;

	removeAll();
	
	for(screen = 0; screen<numScreens; screen++) 
		XInstallColormap(display, DefaultColormap(display, screen));

	if(usingContextServer)
		contextClose();

	XCloseDisplay(display);
	exit(0);
}

// Get the screen number for a given root window
int getScreen(Window root) {
	int i;

	for(i=0; i<numScreens; i++) {
		if(screens[i].root == root)
			return i;
	}

	return -1;
}

void
usage() {
	printf("Usage: anduin <-d displayname> <conf file>");
	exit(1);
}

// Main 
int main(int argc, char *argv[]) {
	char c;
	char *displayvar = "";
	XEvent event;
	int screen;
	Window root;
	int numWins;
	Window winRoot, winParent;
	Window *winChildren;
	XSetWindowAttributes setAttr;
	int i;
	AppWin *appWin;
	XColor color, exact;
	unsigned long valuemask = 0; /* ignore XGCvalues and use defaults */
	XGCValues values;
	Font sandboxFontId, appletFontId, taskFontId, tickerFontId;
	int textHeight;
	XFontStruct *fontInfo;
	int taskHeight, tickerHeight;
	XWindowChanges xwc;

	while ((c = getopt(argc, argv, "d:")) != -1) {
		switch(c) {
		case 'd':
			setenv("DISPLAY", optarg, 1);
			break;
		case '?':
			usage();
		}
	}

	if(optind < argc)
		parseResources(resourceList, numResources, argv[optind]);
	else 
		parseResources(resourceList, numResources, NULL);

	if(rVERBOSE) dumpResources(resourceList, numResources);

	XInitThreads();

	// Connect to the X server
	display = XOpenDisplay(NULL);
	if(display == NULL) {
		printf("Error opening display\n");
		exit(1);
	}
	if(rVERBOSE) printf("Display opened\n");

	taskGC = NULL;
	tickerGC = NULL;

	initTicker();

	XSetErrorHandler(errorHandler);
	// signal(SIGTERM, shutdownWM);
	// signal(SIGINT, shutdownWM);
	// signal(SIGHUP, shutdownWM);
	signal(SIGCHLD, waitForChild);

	// attempt to set the redirect mask early to determine is another window manger is already running
	screen = 0;
	setAttr.event_mask = SubstructureRedirectMask;
	XChangeWindowAttributes(display, RootWindow(display, screen), CWEventMask, &setAttr);

	// Get the number of X screens
	numScreens = ScreenCount(display);
	if(rVERBOSE) printf("number of screens: %d\n", numScreens);
	screens = (ScreenData*) malloc(sizeof(ScreenData));

	// fix me : sdunn - need to generate a copy of each screen specific var
	// currently assuming one screen for most things - I don't
	// know how to get multiple screens yet, but apparently it is possible 

	// generate environment for each screen
	for(screen = 0; screen<numScreens; screen++) {
		root = RootWindow(display, screen);

		screens[screen].screen = screen;
		screens[screen].root = root;
	
		// Test Mode
		if(rMICRO_SIZE) {
			rootWidth = 320;
			rootHeight = 240;
		}
		else {
			rootWidth = DisplayWidth(display, screen); 
			rootHeight = DisplayHeight(display, screen);
		}

		// Move the pointer out of the way for machines that have no mouse to do it themselves
		if(rMOVE_MOUSE)
			XWarpPointer(display, None, root, 0, 0, 0, 0, 0, 0);

		if(rVERBOSE) printf("Screen %d width: %d height: %d\n", 
			screen, rootWidth, rootHeight);

		// create sandbox window
		sandboxWin = XCreateSimpleWindow(display, root, 
			0, 0, 1, 1,
			1, BlackPixel(display, screen), WhitePixel(display, screen));
		sandboxGC = XCreateGC(display, sandboxWin, valuemask, &values);
		sandboxFontId = XLoadFont(display, rFONT_NAME);
		XSetFont(display, sandboxGC, sandboxFontId);

		XStoreName(display, sandboxWin, "SandboxWin");

		XAllocNamedColor(display, DefaultColormap(display, screen), 
			rACTIVE_COLOR, &color, &exact);
		screens[screen].active = color.pixel;

		XAllocNamedColor(display, DefaultColormap(display, screen), 
			rINACTIVE_COLOR, &color, &exact);
		screens[screen].inactive = color.pixel;

		XAllocNamedColor(display, DefaultColormap(display, screen), 
			rAPPLET_BG_COLOR, &color, &exact);
		screens[screen].appletBG = color.pixel;

		XAllocNamedColor(display, DefaultColormap(display, screen), 
			rBORDER_COLOR, &color, &exact);
		screens[screen].borderColor = color.pixel;

		// set root background
		if(strcmp(rBACKGROUND_COLOR, "none") != 0) {
			XAllocNamedColor(display, DefaultColormap(display, screen), 
				rBACKGROUND_COLOR, &color, &exact);
			XSetWindowBackground(display, root, color.pixel);
			XClearWindow(display, root);
		}

		// determine text height and appletWidth
		appletFontId = XLoadFont(display, rFONT_NAME);
		fontInfo = XQueryFont(display, appletFontId);
		textHeight = fontInfo->ascent+fontInfo->descent;
		appletWidth = rootWidth * rAPPLET_RATIO;
		taskWidth = rootWidth-2*rWINDOW_OFFSET-appletWidth-rWINDOW_SPACING, 

		// determine text height and taskHeight
		taskFontId = XLoadFont(display, rFONT_NAME);
		fontInfo = XQueryFont(display, taskFontId);
		textHeight = fontInfo->ascent+fontInfo->descent;
		taskHeight = textHeight+2*rBORDER_WIDTH;

		// determine text height and tickerHeight
		tickerFontId = XLoadFont(display, rFONT_NAME);
		fontInfo = XQueryFont(display, tickerFontId);
		textHeight = fontInfo->ascent+fontInfo->descent;
		tickerHeight = textHeight*rNUM_TICKER_LINES+2*rBORDER_WIDTH;

		// calc standard window size
		windowWidth = rootWidth - appletWidth 
			- 2*rWINDOW_OFFSET - rWINDOW_SPACING;
		windowHeight = rootHeight - tickerHeight - 2*rWINDOW_OFFSET 
			- taskHeight - 2*rWINDOW_SPACING;

		// setup applet window
		appletWin = XCreateSimpleWindow(display, sandboxWin, 
			0, 0,
			appletWidth, 
			rootHeight-2*rWINDOW_OFFSET-tickerHeight-rWINDOW_SPACING, 
			1, BlackPixel(display, screen), WhitePixel(display, screen));
		XReparentWindow(display, appletWin, root, 
			rootWidth-rWINDOW_OFFSET-appletWidth, rWINDOW_OFFSET);

		xwc.border_width = rBORDER_THICKNESS;
		XConfigureWindow(display, appletWin, CWBorderWidth, &xwc);

		XSetWindowBorder(display, appletWin, screens[screen].borderColor);
		XStoreName(display, appletWin, "AppletWin");

		appletGC = XCreateGC(display, appletWin, valuemask, &values);
		XSetFont(display, appletGC, appletFontId);

		XSelectInput(display, appletWin, ExposureMask | SubstructureNotifyMask | SubstructureRedirectMask);
		XMapWindow(display, appletWin);

		XAllocNamedColor(display, DefaultColormap(display, screen), rAPPLET_FG_COLOR, &color, &exact);
		XSetForeground(display, appletGC, color.pixel);
		XAllocNamedColor(display, DefaultColormap(display, screen), 
			rAPPLET_BG_COLOR, &color, &exact);
		XSetWindowBackground(display, appletWin, color.pixel);
		XClearWindow(display, appletWin);

		// setup task window
		taskWin = XCreateSimpleWindow(display, sandboxWin, 
			0, 0,
			taskWidth,
			taskHeight,
			1, BlackPixel(display, screen), WhitePixel(display, screen));
		XReparentWindow(display, taskWin, root, 
			rWINDOW_OFFSET, 
			rootHeight-tickerHeight-rWINDOW_OFFSET-taskHeight-rWINDOW_SPACING);

		xwc.border_width = rBORDER_THICKNESS;
		XConfigureWindow(display, taskWin, CWBorderWidth, &xwc);

		XSetWindowBorder(display, taskWin, screens[screen].borderColor);
		XStoreName(display, taskWin, "TaskWin");

		taskGC = XCreateGC(display, taskWin, valuemask, &values);
		XSetFont(display, taskGC, taskFontId);

		XSelectInput(display, taskWin, ExposureMask);
		XMapWindow(display, taskWin);

		XAllocNamedColor(display, DefaultColormap(display, screen), rACTIVE_TASK_COLOR, &color, &exact);
		XSetForeground(display, taskGC, color.pixel);
		XAllocNamedColor(display, DefaultColormap(display, screen), 
			rTASK_BG_COLOR, &color, &exact);
		XSetWindowBackground(display, taskWin, color.pixel);
		XClearWindow(display, taskWin);

		// setup ticker window
		tickerWin = XCreateSimpleWindow(display, sandboxWin, 
			0, 0,
			rootWidth-2*rWINDOW_OFFSET, tickerHeight,
			1, BlackPixel(display, screen), WhitePixel(display, screen));
		XReparentWindow(display, tickerWin, root, 
			rWINDOW_OFFSET, rootHeight-rWINDOW_OFFSET-tickerHeight);

		xwc.border_width = rBORDER_THICKNESS;
		XConfigureWindow(display, tickerWin, CWBorderWidth, &xwc);

		XSetWindowBorder(display, tickerWin, screens[screen].borderColor);
		XStoreName(display, tickerWin, "TickerWin");

		tickerGC = XCreateGC(display, tickerWin, valuemask, &values);
		XSetFont(display, tickerGC, tickerFontId);

		XSelectInput(display, tickerWin, ExposureMask | PropertyChangeMask);
		XMapWindow(display, tickerWin);

		XAllocNamedColor(display, DefaultColormap(display, screen), rTICKER_FG_COLOR, &color, &exact);
		XSetForeground(display, tickerGC, color.pixel);
		XAllocNamedColor(display, DefaultColormap(display, screen), 
			rTICKER_BG_COLOR, &color, &exact);
		XSetWindowBackground(display, tickerWin, color.pixel);
		XClearWindow(display, tickerWin);

		// Find all the pre-existing windows when the window manager starts
		// once found, add to list and reparent
		XQueryTree(display, root, &winRoot, &winParent, &winChildren, &numWins);
		if(rVERBOSE) printf("%d windows in screen %d\n", numWins, numScreens);
		for(i=0; i<numWins; i++) {
			if(winChildren[i] == sandboxWin || 
				winChildren[i] == appletWin || 
				winChildren[i] == taskWin || 
				winChildren[i] == tickerWin) {
				continue;
			}

			appWin = addWin(winChildren[i]);
			if(appWin != NULL && appWin->isMapped) {
				classifyWin(appWin);
			}
		}
		showAppList();
		reFocus();

		// setup event handling for root window
		setAttr.event_mask = 
			SubstructureRedirectMask | 
			SubstructureNotifyMask |
			PropertyChangeMask | 
			ColormapChangeMask |
			KeyPressMask;
		XChangeWindowAttributes(display, root, CWEventMask, &setAttr);

		// reserve window manager key commands
		grabKeys(root);
	}

	startingWM = 0;

	contextChangeAtom = XInternAtom(display, WM_CONTEXT_CHANGE, False);

	if(rSINGLE_THREAD) {
		// printf("Single Threaded\n");
		contextInit();
	}
	else {
		// printf("Multi Threaded\n");
		if(rCONTEXT_SERVER != NULL)
			contextListen();
	}

	if(rSTART_XTERM && mainAppList == NULL)
		doCommand(CMD_NEW_TERM, NULL);

	// handle events
	while(1) {
		XNextEvent(display, &event);

		if(rSINGLE_THREAD && contextChanged()) {
			readContext();
		}
		dispatch(&event);
	}

	return 0;
}

