/*
 * resourceList.c
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Take care of the user configurable vars
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "anduin.h"
#include "resources.h"

int rVERBOSE;
int rDEFAULT_PRIORITY;
int rIGNORE_TRANSIENTS;
int rTRANSIENTS_TO_APPLETS;
int rMAX_TASK_LENGTH;
int rMAX_TASKS_LISTED;
int rNUM_TICKER_LINES;
int rMAX_TICKER_LENGTH;
char *rFONT_NAME;
int rBORDER_WIDTH;
int rWINDOW_OFFSET;
int rWINDOW_SPACING;
int rAPPLET_SPACING;
int rAPPLET_BORDER;
int rAPPLET_BORDER_ACTIVE;
float rAPPLET_RATIO;
int rAPPLET_HEIGHT;
int rTASK_TEXT_PADDING;
int rTASK_TEXT_SPACING;
int rTASK_TEXT_OFFSET;
char *rACTIVE_COLOR;
char *rINACTIVE_COLOR;
char *rBACKGROUND_COLOR;
char *rACTIVE_TITLE_COLOR;
char *rINACTIVE_TITLE_COLOR;
char *rAPPLET_FG_COLOR;
char *rAPPLET_BG_COLOR;
char *rACTIVE_TASK_COLOR;
char *rINACTIVE_TASK_COLOR;
char *rTASK_BG_COLOR;
char *rTICKER_FG_COLOR;
char *rTICKER_BG_COLOR;
char *rCONTEXT_SERVER;
char *rKEY_FILE;
int rMICRO_SIZE;
int rSINGLE_THREAD;
int rTICKER_TEXT_OUTPUT;
char* rBORDER_COLOR;
int rBORDER_THICKNESS;
int rMOVE_MOUSE;
int rSTART_XTERM;

int numResources = 41;

Resource resourceList[] = {
{"VERBOSE",               &rVERBOSE,               RESOURCE_BOOLEAN, "FALSE"},
{"DEFAULT_PRIORITY",      &rDEFAULT_PRIORITY,      RESOURCE_INT,     "5"},
{"IGNORE_TRANSIENTS",     &rIGNORE_TRANSIENTS,     RESOURCE_BOOLEAN, "FALSE"},
{"TRANSIENTS_TO_APPLETS", &rTRANSIENTS_TO_APPLETS, RESOURCE_BOOLEAN, "FALSE"},
{"MAX_TASK_LENGTH",       &rMAX_TASK_LENGTH,       RESOURCE_INT,     "128"},
{"MAX_TASKS_LISTED",      &rMAX_TASKS_LISTED,      RESOURCE_INT,     "128"},
{"NUM_TICKER_LINES",      &rNUM_TICKER_LINES,      RESOURCE_INT,     "5"},
{"MAX_TICKER_LENGTH",     &rMAX_TICKER_LENGTH,     RESOURCE_INT,     "256"},
{"FONT_NAME",             &rFONT_NAME,             RESOURCE_STRING,  "fixed"},
{"BORDER_WIDTH",          &rBORDER_WIDTH,          RESOURCE_INT,     "5"},
{"WINDOW_OFFSET",         &rWINDOW_OFFSET,         RESOURCE_INT,     "10"},
{"WINDOW_SPACING",        &rWINDOW_SPACING,        RESOURCE_INT,     "10"},
{"APPLET_SPACING",        &rAPPLET_SPACING,        RESOURCE_INT,     "5"},
{"APPLET_BORDER",         &rAPPLET_BORDER,         RESOURCE_INT,     "1"},
{"APPLET_BORDER_ACTIVE",  &rAPPLET_BORDER_ACTIVE,  RESOURCE_INT,     "2"},
{"APPLET_RATIO",          &rAPPLET_RATIO,          RESOURCE_FLOAT,   "0.25"},
{"APPLET_HEIGHT",         &rAPPLET_HEIGHT,         RESOURCE_INT,     "50"},
{"TASK_TEXT_PADDING",     &rTASK_TEXT_PADDING,     RESOURCE_INT,     "2"},
{"TASK_TEXT_SPACING",     &rTASK_TEXT_SPACING,     RESOURCE_INT,     "5"},
{"TASK_TEXT_OFFSET",      &rTASK_TEXT_OFFSET,      RESOURCE_INT,     "5"},
{"ACTIVE_COLOR",          &rACTIVE_COLOR,          RESOURCE_STRING,  "blue"},
{"INACTIVE_COLOR",        &rINACTIVE_COLOR,        RESOURCE_STRING,  "grey"},
{"ACTIVE_TITLE_COLOR",    &rACTIVE_TITLE_COLOR,    RESOURCE_STRING,  "white"},
{"BACKGROUND_COLOR",      &rBACKGROUND_COLOR,      RESOURCE_STRING,  "none"},
{"INACTIVE_TITLE_COLOR",  &rINACTIVE_TITLE_COLOR,  RESOURCE_STRING,  "black"},
{"APPLET_FG_COLOR",       &rAPPLET_FG_COLOR,       RESOURCE_STRING,  "black"},
{"APPLET_BG_COLOR",       &rAPPLET_BG_COLOR,       RESOURCE_STRING,  "dark red"},
{"ACTIVE_TASK_COLOR",     &rACTIVE_TASK_COLOR,     RESOURCE_STRING,  "white"},
{"INACTIVE_TASK_COLOR",   &rINACTIVE_TASK_COLOR,   RESOURCE_STRING,  "black"},
{"TASK_BG_COLOR",         &rTASK_BG_COLOR,         RESOURCE_STRING,  "dark red"},
{"TICKER_FG_COLOR",       &rTICKER_FG_COLOR,       RESOURCE_STRING,  "white"},
{"TICKER_BG_COLOR",       &rTICKER_BG_COLOR,       RESOURCE_STRING,  "dark blue"},
{"MICRO_SIZE",            &rMICRO_SIZE,            RESOURCE_BOOLEAN, "FALSE"},
{"CONTEXT_SERVER",        &rCONTEXT_SERVER,        RESOURCE_STRING,  NULL},
{"KEY_FILE",              &rKEY_FILE,              RESOURCE_STRING,  ".keyFile"},
{"SINGLE_THREAD",         &rSINGLE_THREAD,         RESOURCE_BOOLEAN, "FALSE"},
{"TICKER_TEXT_OUTPUT",    &rTICKER_TEXT_OUTPUT,    RESOURCE_BOOLEAN, "FALSE"},
{"BORDER_COLOR",          &rBORDER_COLOR,          RESOURCE_STRING,  "white"},
{"BORDER_THICKNESS",      &rBORDER_THICKNESS,      RESOURCE_INT,     "1"},
{"MOVE_MOUSE",            &rMOVE_MOUSE,            RESOURCE_BOOLEAN, "FALSE"},
{"START_XTERM",           &rSTART_XTERM,           RESOURCE_BOOLEAN, "FALSE"},
};
