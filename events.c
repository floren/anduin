/*
 * events.c
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Handle all the X Windows events
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/Xatom.h>

#include "anduin.h"

//---------------------------------------------------------

void handleConfigureRequest(XEvent *event) {
	XConfigureRequestEvent *confEvent;
	AppWin *appWin;
	XWindowChanges xwc;

	confEvent = &event->xconfigurerequest;

	appWin = getWin(confEvent->window);
	if(appWin == NULL) {
		tickerMsg("Error finding window for reconfigure");

		xwc.x = confEvent->x;
		xwc.y = confEvent->y;
		xwc.width = confEvent->width;
		xwc.height = confEvent->height;
		XConfigureWindow(display, confEvent->window,
			CWX | CWY | CWWidth | CWHeight, &xwc);
	}
	else {

		// printf("Configure Request %s\n", appWin->name);
		if(appWin->mode == MAIN_APP || appWin->mode == APPLET) {
			if(rVERBOSE) tickerMsg("reconfiguring main app window");
			appWin->x = confEvent->x;
			appWin->y = confEvent->y;
			appWin->width = confEvent->width;
			appWin->height = confEvent->height;
			// XClearWindow(display, appWin->win);
		}
		else {
	   		if(rVERBOSE) tickerMsg("reconfiguring unparented window");
			// printf("Configureing unassigned window %d\n", appWin->isMapped);
			appWin->x = confEvent->x;
			appWin->y = confEvent->y;
			appWin->width = confEvent->width;
			appWin->height = confEvent->height;

			xwc.x = appWin->x;
			xwc.y = appWin->y;
			xwc.width = appWin->width;
			xwc.height = appWin->height;
			XConfigureWindow(display, appWin->win,
				CWX | CWY | CWWidth | CWHeight, &xwc);
		}
	}
}

//---------------------------------------------------------

void handleConfigureNotify(XEvent *event) {
	XConfigureEvent *confEvent;
	AppWin *appWin;

	confEvent = &event->xconfigure;

	appWin = getWin(confEvent->window);
	if(appWin == NULL) 
		return;

	printf("Configure size %dx%d - %s %d\n", confEvent->width, confEvent->height, appWin->name, appWin->isMapped);
}

//---------------------------------------------------------

void handleMapRequest(XEvent *event) {
	AppWin *appWin;
	XMapRequestEvent *mapReqEvent;

	mapReqEvent = &event->xmaprequest;

	appWin = getWin(mapReqEvent->window);
	if(appWin == NULL) 
		appWin = getWinFromParent(mapReqEvent->window);

	if(appWin == NULL) {
		char buffer[64];

		sprintf(buffer, "MapRequest: Could not find window %ld", mapReqEvent->window);
		tickerMsg(buffer);
	}
	else {
		if(rVERBOSE) tickerMsg("Map Request");
		appWin->isMapped = 1;
		if(appWin->mode == UNASSIGNED) {
			classifyWin(appWin);
		}
		else if(appWin->mode == MAIN_APP) {
			addToAppList(&mainAppList, appWin);
			XMapWindow(display, mapReqEvent->window);
		}
		else if(appWin->mode == APPLET) {
			addToAppList(&appletList, appWin);
			XMapWindow(display, mapReqEvent->window);
		}
	}

	if(appWin != NULL && appWin->mode == MAIN_APP)
		reFocus();

	showAppList();
}

//---------------------------------------------------------

void handleMapNotify(XEvent *event) {
	AppWin *appWin;
	XMapEvent *mapEvent;

	mapEvent = &event->xmap;

	appWin = getWin(mapEvent->window);
	if(appWin == NULL) 
		appWin = getWinFromParent(mapEvent->window);

	if(appWin == NULL) {
		char buffer[64];

		sprintf(buffer, "MapNotify: Could not find window %ld", mapEvent->window);
		tickerMsg(buffer);
	}
	else {
		if(rVERBOSE) tickerMsg("Map Notify");
		appWin->isMapped = 1;

		if(appWin->mode == MAIN_APP) {
			XMapWindow(display, appWin->parent);
			addToAppList(&mainAppList, appWin);
			reFocus();
		}
		else if(appWin->mode == APPLET) {
			addToAppList(&appletList, appWin);
			reorderApplets();
		}

		showAppList();
	}
}

//---------------------------------------------------------

void handleUnmapNotify(XEvent *event) {
	AppWin *appWin;
	XUnmapEvent *unmapEvent;

	unmapEvent = &event->xunmap;

	appWin = getWin(unmapEvent->window);
	if(appWin == NULL) {
		appWin = getWinFromParent(unmapEvent->window);
		if(appWin == NULL) {
			// char buffer[64];

			// The unmap event may not show up until after the other cleanup is finished
			//--------------------------------------------------------------------------
			// sprintf(buffer, "UnMapNotify: Could not find window %ld", unmapEvent->window);
			// tickerMsg(buffer);
		}
		else {
			// char buffer[256];

			// sprintf(buffer, "Unmap request from parent: %s", appWin->name);
		 	// tickerMsg(buffer);
		}
	}
	else {
		appWin->isMapped = 0;
		if(rVERBOSE) tickerMsg("Unmapping window");

		if(appWin->mode == MAIN_APP) {
			XUnmapWindow(display, appWin->parent);
			removeFromAppList(&mainAppList, appWin);
		}
		else if(appWin->mode == APPLET) {
			removeFromAppList(&appletList, appWin);
			reorderApplets();
		}

		if(appWin == mainAppList) {
			nextWin();
		}
		showAppList();
		reFocus();
	}
}

//---------------------------------------------------------

int getWinProtocols(AppWin *appWin) {
	int numProto;
	Atom *proto;
	int i;
	Atom takeFocus, deleteWin, saveYourself;

	if(appWin == NULL)
		return 0;

	takeFocus = XInternAtom(display, "WM_TAKE_FOCUS", False);
	deleteWin = XInternAtom(display, "WM_DELETE_WINDOW", False);
	saveYourself = XInternAtom(display, "WM_SAVE_YOURSELF", False);

	appWin->focusNotify = 0;
	appWin->deleteNotify = 0;
	appWin->saveNotify = 0;

	if(rVERBOSE) tickerMsg("WM Protocols changed");
	if(XGetWMProtocols(display, appWin->win, &proto, &numProto) != 0) {
		for(i=0; i<numProto; i++) {
			if(proto[i] == takeFocus) {
				// tickerMsg("Focus Notify");
				appWin->focusNotify = 1;
			}
			else if(proto[i] == deleteWin) {
				// tickerMsg("Delete Notify");
				appWin->deleteNotify = 1;
			}
			else if(proto[i] == saveYourself) {
				// tickerMsg("Save Notify");
				appWin->saveNotify = 1;
			}
			else {
				// tickerMsg("Unknown WM protocol");
			}
		}
		XFree(proto);

		return 1;
	}

	return 0;
}

//---------------------------------------------------------

int getWinName(AppWin *appWin) {
	Atom actual_type_return;
	int actual_format_return;
	unsigned long nitems_return;
	unsigned long bytes_after_return;
	char *data;
	int status;

	if(appWin == NULL)
		return 0;

	status = XGetWindowProperty(display, appWin->win, XA_WM_NAME, 0L, 100L, False, AnyPropertyType,
		&actual_type_return, &actual_format_return, &nitems_return, &bytes_after_return, 
		(unsigned char **) &data);

	if(status == Success && 
	   actual_type_return == XA_STRING && 
	   actual_format_return == 8 &&
	   data != NULL &&
	   nitems_return > 0) {

		// printf("nitems: %ld bytes return: %ld\n", nitems_return, bytes_after_return);
		if(nitems_return > 255)
			nitems_return = 255;

		// strncpy(buffer, data, nitems_return);
		// buffer[nitems_return] = '\0';
		// printf("Name: %s\n", buffer);

		if(appWin->name != NULL)
			free(appWin->name);
		appWin->name = (char*) malloc(nitems_return+1);
		strncpy(appWin->name, data, nitems_return);
		appWin->name[nitems_return] = '\0';

		drawTitle(appWin);
		showAppList();
		if(rVERBOSE) tickerMsg("New Name set");

		return 1;
	}
	else {
		// tickerMsg("Unable to get window name");
	}

	return 0;
}

//---------------------------------------------------------

int getWinPriority(AppWin *appWin) {
	Atom actual_type_return;
	int actual_format_return;
	unsigned long nitems_return;
	unsigned long bytes_after_return;
	long *priority;
	int status;
	Atom priorityAtom;

	char buffer[256];

	if(appWin == NULL)
		return 0;

	appWin->priority = rDEFAULT_PRIORITY;

	priorityAtom = XInternAtom(display, WM_ENCHANT_PRIORITY, True);

	if(priorityAtom != None) {
		status = XGetWindowProperty(display, appWin->win, priorityAtom, 0L, 1L, False, XA_INTEGER,
			&actual_type_return, &actual_format_return, &nitems_return, &bytes_after_return,
			(unsigned char **) &priority);

		if(status != Success || actual_type_return == None || actual_type_return != XA_INTEGER) {
			// tickerMsg("Priority value not available");
		}
		else {
			appWin->priority = *priority;

			if(rVERBOSE) {
				sprintf(buffer, "New Applet priority: %ld", *priority);
				tickerMsg(buffer);
			}
			XFree(priority);

			reorderApplets();

			return 1;
		}
	}

	return 0;
}

//---------------------------------------------------------

int getWinAppletTag(AppWin *appWin) {
	Atom actual_type_return;
	int actual_format_return;
	unsigned long nitems_return;
	unsigned long bytes_after_return;
	long *appletProp;
	int status;
	// char buffer[256];
	Atom appletAtom;

	if(appWin == NULL)
		return 0;

	appletAtom = XInternAtom(display, WM_APPLET_TAG, True);

	if(appletAtom != None) {
		status = XGetWindowProperty(display, appWin->win, appletAtom, 0L, 1L, False, XA_INTEGER,
			&actual_type_return, &actual_format_return, &nitems_return, &bytes_after_return,
			(unsigned char **) &appletProp);

		if(status != Success || actual_type_return == None || actual_type_return != XA_INTEGER) {
			// tickerMsg("Failed to get applet tag value");
		}
		else {
			// sprintf(buffer, "Applet Tag found: %ld", *appletProp);
			// tickerMsg(buffer);

			if(*appletProp) {
				makeApplet(appWin);
				XFree(appletProp);

				return 1;
			}
			else {
				makeMainApp(appWin);
				XFree(appletProp);
			}
		}
	}

	return 0;
}

//---------------------------------------------------------

int getWinTransientTag(AppWin *appWin) {
	Atom actual_type_return;
	int actual_format_return;
	unsigned long nitems_return;
	unsigned long bytes_after_return;
	long *transientProp;
	int status;
	// char buffer[256];
	Atom transientAtom;

	if(appWin == NULL)
		return 0;

	transientAtom = XInternAtom(display, WM_TRANSIENT_TAG, True);

	if(transientAtom != None) {
		status = XGetWindowProperty(display, appWin->win, transientAtom, 0L, 1L, False, XA_INTEGER,
			&actual_type_return, &actual_format_return, &nitems_return, &bytes_after_return,
			(unsigned char **) &transientProp);

		if(status != Success || actual_type_return == None || actual_type_return != XA_INTEGER) {
			// tickerMsg("Failed to get transient tag value");
		}
		else {
			// sprintf(buffer, "Transient Tag found: %ld", *transientProp);
			// tickerMsg(buffer);

			if(*transientProp) {
				// sdunn - do something here
				appWin->isTransient = 1;
				XFree(transientProp);

				return 1;
			}
			else {
				// sdunn - do something here
				appWin->isTransient = 0;
				XFree(transientProp);
			}
		}
	}

	return 0;
}

//---------------------------------------------------------

void handlePropertyChange(XEvent *event) {
	AppWin *appWin;
	XPropertyEvent *propEvent;
	Atom appletAtom;
	Atom priorityAtom;
	Atom protocolsAtom;
	Atom transientAtom;

	propEvent = &event->xproperty;

	if(rVERBOSE) tickerMsg("Property Changed");

	if(propEvent->window == tickerWin && propEvent->atom == contextChangeAtom) {
		// tickerMsg("Context Change");
		readContext();
		return;
	}

	appWin = getWin(propEvent->window);
	if(appWin == NULL) {
		// tickerMsg("PropertyChange: Could not find window"); // sdunn
	}
	else {
		appletAtom = XInternAtom(display, WM_APPLET_TAG, True);
		priorityAtom = XInternAtom(display, WM_ENCHANT_PRIORITY, True);
		protocolsAtom = XInternAtom(display, "WM_PROTOCOLS", True);
		transientAtom = XInternAtom(display, WM_TRANSIENT_TAG, True);

		if(propEvent->atom == XA_WM_NAME) {
			// tickerMsg("Name Change");
			getWinName(appWin);
		}
		else if(appletAtom != None && propEvent->atom == appletAtom) {
			// tickerMsg("AppletTag Change");
			getWinAppletTag(appWin);
		}
		else if(priorityAtom != None && propEvent->atom == priorityAtom) {
			// tickerMsg("Priority Change");
			getWinPriority(appWin);
		}
		else if(protocolsAtom != None && propEvent->atom == protocolsAtom) {
			// tickerMsg("Protocols Change");
			getWinProtocols(appWin);
		}
		else if(transientAtom != None && propEvent->atom == transientAtom) {
			// tickerMsg("Transient Change");
			getWinTransientTag(appWin);
		}
		else {
			// tickerMsg("Unknown Prop Change");
			if(rVERBOSE) tickerMsg("Unknown Property Changed");
		}
	}
}

//---------------------------------------------------------

void handleCreateNotify(XEvent *event) {
	XCreateWindowEvent *createEvent;
	char buffer[256];
	AppWin *appWin;

	createEvent = &event->xcreatewindow;

	appWin = getWin(createEvent->window);
	if(appWin != NULL) {
		return;
	}
	else {
		appWin = getWinFromParent(createEvent->window);
		if(appWin != NULL)
			return;
	}

	appWin = addWin(createEvent->window);
	if(appWin == NULL)
		return;

	if(rVERBOSE) {
		if(appWin->name != NULL)
			sprintf(buffer, "New Window: %s x: %d y: %d width: %d height: %d", 
				appWin->name, appWin->x, appWin->y, appWin->width, appWin->height);
		else
			strcpy(buffer, "New Window: unnamed");
		tickerMsg(buffer);
	}
}

//---------------------------------------------------------

void handleReparentNotify(XEvent *event) {
	XReparentEvent *reparentEvent;
	char buffer[256];
	AppWin *appWin;
	int screen = 0;

	reparentEvent = &event->xreparent;

	appWin = getWin(reparentEvent->window);
	if(appWin != NULL) {
		return;
	}
	else {
		appWin = getWinFromParent(reparentEvent->window);
		if(appWin != NULL)
			return;
	}

	if(reparentEvent->parent != screens[screen].root)
		return;
		
	appWin = addWin(reparentEvent->window);
	if(appWin == NULL)
		return;

	if(rVERBOSE) {
		if(appWin->name != NULL)
			sprintf(buffer, "Reparented Window: %s x: %d y: %d width: %d height: %d", 
				appWin->name, appWin->x, appWin->y, appWin->width, appWin->height);
		else
			strcpy(buffer, "Reparented Window: unnamed");
		tickerMsg(buffer);
	}
}

//---------------------------------------------------------

void handleDestroyNotify(XEvent *event) {
	AppWin *appWin;
	XDestroyWindowEvent *destroyEvent;
	char buffer[256];

	destroyEvent = &event->xdestroywindow;
	appWin = getWin(destroyEvent->window);

	if(appWin == NULL) {
		appWin = getWinFromParent(destroyEvent->window);

		if(appWin == NULL) {
			if(!shutdownStarted) {
fprintf(stderr, "Unfound win %ld destroyed\n", destroyEvent->window); // sdunn
			}
			return;
		}
	}

	if(appWin->name != NULL) {
		if(rVERBOSE) {
			sprintf(buffer, "Window: %s destroyed", appWin->name);
			tickerMsg(buffer);
		}
	}
	else {
fprintf(stderr, "Window: Unknown destroyed\n"); // sdunn
		if(rVERBOSE) tickerMsg("Window: Unknown destroyed");
	}

	removeWin(appWin, False);
}

//---------------------------------------------------------

void handleKeyPress(XEvent *event) {
	XKeyEvent *keyEvent;
	int numChars;
	int bufSize = 20;
	KeySym keysym;
	XComposeStatus compose;
	char buffer[256];

	keyEvent = &event->xkey;
	// printf("KeyPress - code: 0x%x modifiers: 0x%x\n", 
		// keyEvent->keycode, keyEvent->state);

	numChars = XLookupString(keyEvent, buffer, bufSize, &keysym,
		&compose);

	// printf("Keysym 0x%lx Char %s\n", keysym, XKeysymToString(keysym));
	doKeyCommand(keysym, keyEvent->state);
}


//---------------------------------------------------------

void handleKeyRelease(XEvent *event) {
	XKeyEvent *keyEvent;
	int numChars;
	int bufSize = 20;
	KeySym keysym;
	XComposeStatus compose;
	char buffer[256];

	keyEvent = &event->xkey;
	// printf("KeyRelease - code: 0x%x modifiers: 0x%x\n", 
		// keyEvent->keycode, keyEvent->state);

	numChars = XLookupString(keyEvent, buffer, bufSize, &keysym,
		&compose);

	// printf("Keysym 0x%lx Char %s\n", keysym, XKeysymToString(keysym));
	releaseKey(keysym, keyEvent->state);
}

//---------------------------------------------------------

void handleExpose(XEvent *event) {
	XExposeEvent *exposeEvent;
	AppWin *appWin;

	exposeEvent = &event->xexpose;
	if(exposeEvent->window == tickerWin) {
		drawTicker();
	}
	else if(exposeEvent->window == taskWin) {
		showAppList();
	}
	else if( (appWin = getWinFromParent(exposeEvent->window)) != NULL) {
		drawTitle(appWin);
	}
}

//---------------------------------------------------------

void handleColormapNotify(XEvent *event) {
	XColormapEvent *colormapEvent;
	AppWin *appWin;

	colormapEvent = &event->xcolormap;
	appWin = getWin(colormapEvent->window);
	if(appWin == NULL)
		return;
	if(colormapEvent->new)
		XInstallColormap(display, colormapEvent->colormap);
}

//---------------------------------------------------------

void dispatch(XEvent *event) {
	switch(event->type) {
		// SubstructureRedirectMask -----
		case CirculateRequest:
			if(rVERBOSE) tickerMsg("Event: CirculateRequest");
			break;
		case ConfigureRequest:
			if(rVERBOSE) tickerMsg("Event: ConfigureRequest");
			handleConfigureRequest(event);
			break;
		case MapRequest:
			if(rVERBOSE) tickerMsg("Event: MapRequest");
			handleMapRequest(event);
			break;

		// SubstructureNotifyMask -------
		case CirculateNotify:
			if(rVERBOSE) tickerMsg("Event: CirculateNotify");
			break;
		case ConfigureNotify:
			if(rVERBOSE) tickerMsg("Event: ConfigureNotify");
			// handleConfigureNotify(event);
			break;
		case CreateNotify:
			if(rVERBOSE) tickerMsg("Event: CreateNotify");
			handleCreateNotify(event);
			break;
		case DestroyNotify:
			if(rVERBOSE) tickerMsg("Event: DestroyNotify");
			handleDestroyNotify(event);
			break;
		case GravityNotify:
			if(rVERBOSE) tickerMsg("Event: GravityNotify");
			break;
		case MapNotify:
			if(rVERBOSE) tickerMsg("Event: MapNotify");
			handleMapNotify(event);
			break;
		case ReparentNotify:
			if(rVERBOSE) tickerMsg("Event: ReparentNotify");
			handleReparentNotify(event);
			break;
		case UnmapNotify:
			if(rVERBOSE) tickerMsg("Event: UnmapNotify");
			handleUnmapNotify(event);
			break;

		// PropertyChangeMask ----------
		case PropertyNotify:
			if(rVERBOSE) tickerMsg("Event: PropertyNotify");
			handlePropertyChange(event);
			break;

		// KeyPressMask ----------------
		case KeyPress:
			if(rVERBOSE) tickerMsg("Event: KeyPress");
			handleKeyPress(event);
			break;

		case KeyRelease:
			if(rVERBOSE) tickerMsg("Event: KeyRelease");
			handleKeyRelease(event);
			break;

		// ExposeMask ---------------------------------------
		case Expose:
			if(rVERBOSE) tickerMsg("Expose Event");
			handleExpose(event);
			break;

		// ColormapNotifyMask ---------------------------------------
		case ColormapNotify:
			if(rVERBOSE) tickerMsg("Colormap Notify Event");
			handleColormapNotify(event);
			break;

		default:
			if(rVERBOSE) {
				char buffer[64];

				sprintf(buffer, "Event: %d", event->type);
				tickerMsg(buffer);
			}
			break;
	}
}


