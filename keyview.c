#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>

#include "anduin.h"

Display *display = NULL;
int numScreens = 0;
ScreenData *screens = NULL;

int main(int argc, char *argv[]) {
	printf("KeyView\n");

	if(argc > 1)
		parseResources(resourceList, numResources, argv[1]);
	else 
		parseResources(resourceList, numResources, NULL);

	printf("\n");

	// dumpResources(resourceList, numResources);
	dumpAllKeys();

	return 0;
}

