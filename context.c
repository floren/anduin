/*
 * context.c
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Provides the interface to the context manager
 *
 * Handle all the input commands that can ge given to Anduin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

#include "anduin.h"


#ifdef USE_CONTEXT
#include "client_ipc.h"
#include "dstring.h"

static int messageWaiting = 0;
#endif

int usingContextServer = 0;

int contextInit() {
#ifdef USE_CONTEXT
	NodeType *node, *branch;
	char buffer[256];
	DataType data;

	// printf("Connecting to Context Server\n");
	strcpy(buffer, "Anduin Window Manager v0.1");
	data.string_val = buffer;
	node = CreateNode("Anduin", T_STRING, strlen(buffer), &data, FALSE);
	InitClientIPC(node);
	DeleteNode(NULL, node);

	branch = CreateBranchNode("Anduin", T_BRANCH, 0);
	
	strcpy(buffer, "Anduin Started");
	data.string_val = buffer;
	node = CreateNode("TickerMessage", T_STRING, strlen(buffer), &data, FALSE);
	InsertNode(branch, node);

	sprintf(buffer, "%s:/Anduin", rCONTEXT_SERVER);
	if(PublishNode(InstanceString(buffer), branch, NULL, TRUE) == FALSE) {
		printf("Could not connect to context server\n");
		return 0;
	}
	else {
		// printf("First Node published\n");

		// sprintf(buffer, "%s:/Anduin", rCONTEXT_SERVER);
		// LockNode(InstanceString(buffer), FALSE, TRUE);

		sprintf(buffer, "%s:/Anduin/TickerMessage", rCONTEXT_SERVER);
		SubscribeNode(InstanceString(buffer), TRUE, TRUE);

		usingContextServer = 1;
		return 1;
	}
#endif
	return 0;
}

void* startContextThread(void *data) {
#ifdef USE_CONTEXT
	int state = 1;

	while(1) {
		// printf("Context Event\n");
		if(messageWaiting) {
			sleep(1);
		}	
		else {
			// printf("Start WaitForNodes\n");
			WaitForNodes();
			// printf("End WaitForNodes\n");
			messageWaiting = 1;

			XChangeProperty(display, tickerWin, contextChangeAtom, XA_INTEGER, 32, 
				PropModeReplace, (unsigned char*) &state, 1);
			XFlush(display);
		}
	}
#endif

	return NULL;
}

void contextListen() {
#ifdef USE_CONTEXT
	pthread_t pthreadId;

	if(contextInit()) {
		if( pthread_create(&pthreadId, NULL, startContextThread, NULL) != 0) {
			tickerMsg("Error creating thread\n");
		}
	}
#endif
}

void readContext() {
#ifdef USE_CONTEXT
	NodeType *senderNode, *mesgNode, *updateNode, *respNode;
	char *unl, *publisher;
	char buffer[256];
	char *callType, *returnUnl, *result;

	if(! rSINGLE_THREAD && ! messageWaiting)
		return;

	// tickerMsg("Read Context");
	if(DequeueMessage(&senderNode, &mesgNode, FALSE) && senderNode != NULL && mesgNode != NULL) {
		if(rVERBOSE) printf("Got Message");
		DeleteNode(NULL, senderNode);
		DeleteNode(NULL, mesgNode);
	}

	if(DequeueUpdate(&unl, &publisher, &updateNode, FALSE) && unl != NULL && updateNode != NULL) {
		// sprintf(buffer, "Got update to '%s'", unl);
		// tickerMsg(buffer);

		if(strcmp(updateNode->name, "TickerMessage") == 0) {
			// tickerMsg("Got New Ticker Message");

			if(IsType(updateNode->type, T_STRING)) {
				sprintf(buffer, "INCOMING '%s'", updateNode->data.string_val);
				drawTickerMsg(buffer);
			}
			else {
				drawTickerMsg("INCOMING context msg not a string");
			}
		}
		DeleteNode(NULL, updateNode);
	}

	if(DequeueResponse(&callType, &returnUnl, &respNode, &result, FALSE) == TRUE && result != NULL) {
		if(rVERBOSE) printf("Got Response");
		DeleteNode(NULL, respNode);
	}

	messageWaiting = 0;
#endif
}

void contextClose() {
#ifdef USE_CONTEXT
	ShutDownClientIPC();
	usingContextServer = 0;
#endif
}

void sendContextString(char *unl, char *str) {
#ifdef USE_CONTEXT
	DataType data;
	NodeType *node;
	char buffer[256];

	data.string_val = str;
	node = CreateNode("TickerMsg", T_STRING, strlen(data.string_val), &data, FALSE);

	sprintf(buffer, "%s:/Anduin/TickerMessage", rCONTEXT_SERVER);
	PublishNode(buffer, node, NULL, TRUE);
#endif
}

int contextChanged() {
#ifdef USE_CONTEXT
	return (PeekNodes() == TRUE);
#else
	return 0;
#endif
}
