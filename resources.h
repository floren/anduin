/*
 * resources.h
 * Author: Steve Dunn, BorgLab MIT 
 * copyright (C) 2001 MIT Media Lab
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// Commands
#define CMD_NONE -1
#define CMD_MODE 0
#define CMD_END_MODE 1
#define CMD_NEW_TERM 2
#define CMD_KILL_WIN 3
#define CMD_SHUTDOWN 4
#define CMD_FULL_SIZE 5
#define CMD_STD_SIZE 6
#define CMD_MAKE_APPLET 7
#define CMD_CHATTER 8
#define CMD_START_APP 9
#define CMD_MAIN_APP 10
#define CMD_APPLET 11
#define CMD_DEBUG 12
#define CMD_PREV_APP 13
#define CMD_NEXT_APP 14

// Resources
extern int rSINGLE_TASK; // limits windows to 1 main app and applets as opposed to overlapping windows
extern int rVERBOSE; // debugging output
extern int rDEFAULT_PRIORITY;
extern int rIGNORE_TRANSIENTS; // allow windows marked transient to exist outside the framework managed by anduin
extern int rTRANSIENTS_TO_APPLETS; // place transient windows in the applet bar as opposed to main window
extern int rMAX_TASK_LENGTH;
extern int rMAX_TASKS_LISTED;
extern int rNUM_TICKER_LINES;
extern int rMAX_TICKER_LENGTH;
extern char *rFONT_NAME;
extern int rBORDER_WIDTH; // padding around windows
extern int rWINDOW_OFFSET; // padding between WM windows and screen edge
extern int rWINDOW_SPACING; // padding between WM windows
extern int rAPPLET_SPACING; // padding between applets
extern int rAPPLET_BORDER; // standard applet border width
extern int rAPPLET_BORDER_ACTIVE; // applet border width when the applet is selected
extern float rAPPLET_RATIO; // fraction of the screen width used by applet window
extern int rAPPLET_HEIGHT; // height of each applet
extern int rTASK_TEXT_PADDING; // space from text to bounding box
extern int rTASK_TEXT_SPACING; // spacing between task names
extern int rTASK_TEXT_OFFSET; // text offset from window border

extern char *rACTIVE_COLOR; // color of the active windows background
extern char *rINACTIVE_COLOR; // color of the inactive windows background
extern char *rBACKGROUND_COLOR; // color of the root window background

extern char *rACTIVE_TITLE_COLOR; // color of the active windows title
extern char *rINACTIVE_TITLE_COLOR; // color of the inactive windows title

extern char *rAPPLET_FG_COLOR; // foreground color of the applet window
extern char *rAPPLET_BG_COLOR; // background color of the applet window
extern char *rACTIVE_TASK_COLOR; // active text color of the task window
extern char *rINACTIVE_TASK_COLOR; // inactive text color of the task window
extern char *rTASK_BG_COLOR; // background color of the task window
extern char *rTICKER_FG_COLOR; // foreground color of the ticker window
extern char *rTICKER_BG_COLOR; // background color of the ticker window
extern int rMICRO_SIZE;
extern char *rCONTEXT_SERVER;
extern char *rKEY_FILE;
extern int rSINGLE_THREAD;
extern int rTICKER_TEXT_OUTPUT;

extern char* rBORDER_COLOR;
extern int rBORDER_THICKNESS;
extern int rMOVE_MOUSE;
extern int rSTART_XTERM;

