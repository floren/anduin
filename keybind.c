/*
 * commands.c
 * Author: Steve Dunn, MIT Wearable Computing Group
 * copyright (C) 2001 MIT Media Lab
 *
 * Handle all the input commands that can ge given to Anduin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/keysym.h>

#include "anduin.h"
#include "resources.h"

#define MAX_BUF_SIZE 128

int numCmds = 15;
Command commandList[] = {
{ XK_q, ControlMask | Mod1Mask, CMD_SHUTDOWN, "CMD_SHUTDOWN" },
{ XK_Alt_L, 0, CMD_MODE, "CMD_MODE" },
{ XK_t, 0, CMD_NEW_TERM, "CMD_NEW_TERM" },
{ XK_k, 0, CMD_KILL_WIN, "CMD_KILL_WIN" },
{ XK_f, 0, CMD_FULL_SIZE, "CMD_FULL_SIZE" },
{ XK_s, 0, CMD_STD_SIZE, "CMD_STD_SIZE" },
{ XK_a, 0, CMD_MAKE_APPLET, "CMD_MAKE_APPLET" },
{ XK_c, 0, CMD_CHATTER, "CMD_CHATTER" },
{ XK_l, 0, CMD_START_APP, "CMD_START_APP" },
{ XK_z, 0, CMD_MAIN_APP, "CMD_MAIN_APP" },
{ XK_m, 0, CMD_APPLET, "CMD_APPLET" },
{ XK_d, 0, CMD_DEBUG, "CMD_DEBUG" },
{ XK_p, 0, CMD_PREV_APP, "CMD_PREV_APP" },
{ XK_n, 0, CMD_NEXT_APP, "CMD_NEXT_APP" },
{ XK_VoidSymbol, 0, CMD_END_MODE, "CMD_MODE" },
};

static KeyBind *cmdMode = NULL;
static KeyBind *modeRoot = NULL;
static unsigned int cmdMask = 0;

int strToSymbol(char *str) {
	int i;

	for(i=0; i<numCmds; i++) {
		if(strcmp(commandList[i].str, str) == 0)
			return commandList[i].cmd;
	}

	return CMD_NONE;
}

char* symbolToStr(int symbol) {
	int i;

	for(i=0; i<numCmds; i++) {
		if(commandList[i].cmd == symbol)
			return commandList[i].str;
	}

	return NULL;
}

char* maskToStr(unsigned int mask) {
	if(mask == ControlMask)
		return "Ctrl";
	else if(mask == Mod1Mask)
		return "Alt";
	else if(mask == (ControlMask | Mod1Mask))
		return "Ctrl,Alt";
	else
		return "";
}

KeyBind* setKey(char *name, char *value) {
	KeySym keySym;
	int symbol;
	KeyBind *keyBind;
	char *ptr;
	char seperator = ','; // this prevents ',' as an actual command

	ptr = strchr(value, seperator);
	if(ptr != NULL) {
		*ptr = '\0';
		ptr++;
	}

	keySym = XStringToKeysym(value);
	symbol = strToSymbol(name);
	// printf("Name: %s Value: '%s' 0x%lx - %d\n", name, value, keySym, symbol);

	keyBind = (KeyBind*) malloc(sizeof(KeyBind));
	keyBind->cmd = symbol;
	keyBind->keySym = keySym;
	keyBind->keys = NULL;
	keyBind->parent = NULL;

	keyBind->mask = 0;
	while(ptr != NULL && *ptr != '\0') {
		value = ptr;

		ptr = strchr(value, seperator);
		if(ptr != NULL) {
			*ptr = '\0';
			ptr++;
		}

		if(strcmp(value, "Ctrl") == 0) {
			keyBind->mask |= ControlMask;
		}
		else if(strcmp(value, "Alt") == 0) {
			keyBind->mask |= Mod1Mask;
		}
	}

	keyBind->next = NULL;

	return keyBind;
}

void parseKeys(char *filename) {
	FILE *fp;
	char buffer[MAX_BUF_SIZE+1];
	char *ptr, *arg;
	KeyBind *newKey;

printf("Reading key file: %s\n", filename);
	if(cmdMode == NULL) {
   		modeRoot = setKey("CMD_MODE", "");
		modeRoot->parent = modeRoot;
		cmdMode = modeRoot;
	}

	if(filename == NULL)
		return;

	fp = fopen(filename, "r");
	if(fp == NULL) {
		fprintf(stderr, "Error reading config file '%s'\n", filename);
		return;
	}

	while( fgets(buffer, MAX_BUF_SIZE, fp) != NULL) {
		if(*buffer == '#' || *buffer == '\n')
			continue;

		ptr = strchr(buffer, '\n');
		if(ptr != NULL)
			*ptr = '\0';

		arg = strrchr(buffer, '|');
		if(arg != NULL) {
			*arg = '\0';
			arg++;
		}

		if(strcmp(buffer, "CMD_END_MODE") == 0) {
			cmdMode = cmdMode->parent;
			continue;
		}

		ptr = strchr(buffer, '=');
		if(ptr != NULL) {
			*ptr = '\0';

			newKey = setKey(buffer, ptr+1);
			newKey->next = cmdMode->keys;
			newKey->parent = cmdMode;
			cmdMode->keys = newKey;

			if(arg == NULL) {
				newKey->arg = NULL;
			}
			else {
				newKey->arg = (char*) malloc(strlen(arg)+1);
				strcpy(newKey->arg, arg);
			}

			if(newKey->cmd == CMD_MODE)
				cmdMode = newKey;
		}
		else {
			printf("Bad Entry: %s", buffer);
		}
	}

	fclose(fp);

	// dumpKeys(cmdMode, 0);
}

int findKey(KeySym keySym, unsigned int mask) {
	KeyBind *ptr;

	ptr = findKeyEntry(keySym, mask);

	if(ptr == NULL)
		return -1;
	else
		return ptr->cmd;
}

KeyBind* findKeyEntry(KeySym keySym, unsigned int mask) {
	KeyBind *ptr;

	if(cmdMode == NULL) {
printf("Empty cmdMode\n");
		return NULL;
	}

	if( (cmdMode->keySym == keySym) && ( (cmdMode->mask | cmdMask) == mask)) {
		// printf("found command %d\n", cmdMode->cmd);
		return cmdMode;
	}

	ptr = cmdMode->keys;

	// printf("findKey - Keysym 0x%lx Char %s\n", keySym, XKeysymToString(keySym));
	while(ptr != NULL) {
		// printf("Looking for %s search %s\n", XKeysymToString(keySym), XKeysymToString(ptr->keySym));
		// printf("Mask: 0x%x search 0x%x\n", mask, ptr->mask);

		if( (ptr->keySym == keySym) && ( (ptr->mask | cmdMask) == mask)) {
			// printf("found command %d\n", ptr->cmd);
			return ptr;
		}

		ptr = ptr->next;
	}
	
	return NULL;
}

KeyBind* findKeyEntryByCmd(int cmd) {
	KeyBind *ptr;

	if(cmd == CMD_NONE) {
printf("Empty cmd str\n");
		return NULL;
	}

	if(cmdMode->cmd == cmd) {
		// printf("found command %d\n", cmdMode->cmd);
		return cmdMode;
	}

	ptr = cmdMode->keys;

	// printf("findKey - Keysym 0x%lx Char %s\n", keySym, XKeysymToString(keySym));
	while(ptr != NULL) {
		// printf("Looking for %s search %s\n", XKeysymToString(keySym), XKeysymToString(ptr->keySym));
		// printf("Mask: 0x%x search 0x%x\n", mask, ptr->mask);

		if(ptr->cmd == cmd) {
			// printf("found command %d\n", ptr->cmd);
			return ptr;
		}

		ptr = ptr->next;
	}
	
	return NULL;
}

void dumpKeys(KeyBind *list, int level) {
	KeyBind *ptr;
	int i;
	char *arg;

	if(list == NULL)
		return;
	ptr = list->keys;

	while(ptr != NULL) {
		for(i=0; i<level; i++)
			putchar(' ');

		if(ptr->arg == NULL)
			arg = "";
		else 
			arg = ptr->arg;

		printf("%s (%s %s) %s\n", 
			symbolToStr(ptr->cmd), 
			maskToStr(ptr->mask), 
			XKeysymToString(ptr->keySym), 
			arg);
		if(ptr->keys)
			dumpKeys(ptr, level+1);
		ptr = ptr->next;
	}
}

void dumpAllKeys() {
	dumpKeys(cmdMode, 0);
}

void grabKeys(Window win) {
	int i;
	KeyBind *ptr;

	// printf("Grabbing Keys\n");
	if(cmdMode == NULL) {
   		modeRoot = setKey("CMD_MODE", "");
		modeRoot->parent = modeRoot;
		cmdMode = modeRoot;
	}
	ptr = cmdMode->keys;

	if(ptr == NULL) {
		for(i=0; i<numCmds; i++) {
			if(commandList[i].cmd == CMD_END_MODE) {
				cmdMode = cmdMode->parent;
			}
			else {
				ptr = (KeyBind*) malloc(sizeof(KeyBind));
				ptr->cmd = commandList[i].cmd;
				ptr->keySym = commandList[i].defaultKeySym;
				ptr->mask = commandList[i].defaultMask;
				ptr->next = cmdMode->keys;
				ptr->parent = NULL;
				ptr->keys = NULL;
				ptr->arg = NULL;
				cmdMode->keys = ptr;

				if(commandList[i].cmd == CMD_MODE) {
					ptr->parent = cmdMode;
					cmdMode = ptr;
				}
			}
		}
	}	

	if(cmdMode->keySym != NoSymbol) {
		// printf("Main Key Grab cmd: %s keySym: %s\n", symbolToStr(cmdMode->cmd), XKeysymToString(cmdMode->keySym));
		XGrabKey(display, XKeysymToKeycode(display, cmdMode->keySym),
			cmdMode->mask, win, True, GrabModeAsync, GrabModeAsync);
	}

	// printf("Default Key Grab cmd: %d keySym: 0x%lx\n", ptr->cmd, ptr->keySym);
	// ptr = (KeyBind*) malloc(sizeof(KeyBind));
	// ptr->cmd = CMD_SHUTDOWN;
	// ptr->keySym = XK_q;
	// ptr->mask = ControlMask | Mod1Mask;
	// ptr->next = cmdMode->keys;
	// cmdMode->keys = ptr;

	ptr = cmdMode->keys;
	while(ptr != NULL) {
		// printf("Key Grab cmd: %s keySym: %s\n", symbolToStr(ptr->cmd), XKeysymToString(ptr->keySym));
		if(ptr->keySym != NoSymbol) {
			XGrabKey(display, XKeysymToKeycode(display, ptr->keySym),
				ptr->mask, win, True, GrabModeAsync, GrabModeAsync);
		}
		ptr = ptr->next;
	}
}

void ungrabKeys(Window win) {
	KeyBind *ptr;

	if(cmdMode == NULL) 
		return;

	if(cmdMode->keySym != NoSymbol) {
		// printf("Main Key Ungrab cmd: %s keySym: %s\n", symbolToStr(ptr->cmd), XKeysymToString(ptr->keySym));
		XUngrabKey(display, XKeysymToKeycode(display, cmdMode->keySym), cmdMode->mask, win);
	}

	ptr = cmdMode->keys;
	while(ptr != NULL) {
		// printf("Key Ungrab cmd: %s keySym: %s\n", symbolToStr(ptr->cmd), XKeysymToString(ptr->keySym));
		if(ptr->keySym != NoSymbol)
			XUngrabKey(display, XKeysymToKeycode(display, ptr->keySym), ptr->mask, win);
		ptr = ptr->next;
	}
}

void switchCmdMode(KeyBind *newMode) {
	KeyBind *ptr;
	int screen;

	for(screen = 0; screen<numScreens; screen++) {
		ungrabKeys(screens[screen].root);

		cmdMask = 0;
		ptr = newMode;
		while(ptr != ptr->parent) {
			if(ptr->keySym == XK_Control_L || ptr->keySym == XK_Control_R)
				cmdMask |= ControlMask;
			else if(ptr->keySym == XK_Alt_L || ptr->keySym == XK_Alt_R)
				cmdMask |= Mod1Mask;
		
			ptr = ptr->parent;
		}

		cmdMode = newMode;
		grabKeys(screens[screen].root);
	}
}

void releaseKey(KeySym keySym, unsigned int mask) {
	int cmd;

	cmd = findKey(keySym, mask);
	if(cmd == CMD_MODE) {
		// printf("Releasing Mode\n");
		switchCmdMode(cmdMode->parent);
	}
}

